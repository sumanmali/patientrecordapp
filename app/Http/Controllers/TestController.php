<?php

namespace App\Http\Controllers;

use App\Test;
use App\TestCategory;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::latest()->get();
        $testcat = TestCategory::latest()->get();
        return view ('test.tests.index',compact('tests','testcat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $testcat = TestCategory::latest()->get();
        return view('test.tests.create',compact('testcat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Test::create($request->all());
        return redirect('/tests');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test,$id)
    {
        $tests = Test::findOrFail($id);
        $testcat = TestCategory::latest()->get();
        return view ('test.tests.edit',compact('tests','testcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test,$id)
    {
        $tests  = Test::findOrFail($id);
        $input = $request->all();
        $tests->fill($input)->save();
        return redirect('/tests');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test,$id)
    {
        $tests  = Test::findOrFail($id)->delete();
        return redirect()->back();
    }
}
