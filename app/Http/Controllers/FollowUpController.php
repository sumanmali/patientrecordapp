<?php

namespace App\Http\Controllers;

use App\FollowUp;
use App\flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\ImageService;


class FollowUpController extends Controller
{
    /**
     * @var Imageservice
     */
    protected $imageService;
    
    /**
     * Slider constructor
     * @param ImageService $imageService 
     * @return type
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function store(Request $request){
    	$input = $request->validate([
            'dctors_name' => 'required',
            'patient_id' => 'required',
            'follow_up' => 'required',
            'advice' => 'required',
            'user_id' => 'required',
            'followup_date' => 'required',
            'signature_photo' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $input['signature_photo'] = $this->imageService->saveImage($request->file('signature_photo'), 'images/signature-images/');
		$followups = FollowUp::create($input);
    	return redirect('/home#follow_up-tab');
    }
}