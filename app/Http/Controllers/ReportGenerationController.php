<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\patientregistration;
use App\TestCategory;
use Carbon\Carbon;
use App\ChiefComplaints;
use App\HistoryIllness;
use App\Pathology;
use App\Test;
use App\Rx;
use App\FollowUp;
use App\OnExamination;

class ReportGenerationController extends Controller
{
    public function overallReport(){
        $patient = patientregistration::where('id', session()->get('patient')->id)
                                        ->orderBy('id', 'DESC')->first();
        $chief_complaints = ChiefComplaints::where('patient_id', session()->get('patient')->id)
                                            ->whereDate('created_at', Carbon::today())->get();
        $history_illness = HistoryIllness::where('patient_id', session()->get('patient')->id)
                                            ->whereDate('created_at', Carbon::today())->get();
        $on_examination = OnExamination::where('patient_id', session()->get('patient')->id)
                                            ->whereDate('created_at', Carbon::today())
                                            ->orderBy('id', 'DESC')->first();
        $pathology = Pathology::where('patient_id', session()->get('patient')->id)
                                ->whereDate('created_at', Carbon::today())->get();
        $rxes = Rx::where('patient_id', session()->get('patient')->id)
                    ->whereDate('created_at', Carbon::today())->get();
        $follow_ups = Followup::where('patient_id', session()->get('patient')->id)
                                ->whereDate('created_at', Carbon::today())->get();
        $tests = Test::all();
        return view('template.overallReport', compact('chief_complaints', 'history_illness',
                     'on_examination', 'pathology', 'rxes', 'follow_ups', 'tests', 'patient'));
    }
}
