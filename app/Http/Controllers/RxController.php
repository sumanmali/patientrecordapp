<?php

namespace App\Http\Controllers;

use App\Rx;
use App\flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RxController extends Controller
{
     public function store(Request $request){
    	Rx::create($request->all());
        // $this->storingSession();
        return redirect('/home#rx-tab');

    }

     public function update(Request $request, $id){
        $input = $request->all();
        $rxes = Rx::findOrFail($id);
        $rxes->update($input);
        return redirect('/home#rx-tab');

    }
    
    public function destroy($id){
        $rxes = Rx::find($id);
        $rxes->delete();
        // $this->storingSession();
        return redirect('/home#rx-tab');
    }
}
