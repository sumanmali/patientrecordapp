<?php

namespace App\Http\Controllers;

use App\patientregistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Gender;
use App\Doctor;
use App\Opd;
use App\ChiefComplaints;
use App\FollowUp;
use App\HistoryIllness;
use App\Investigation;
use App\OnExamination;
use App\Pathology;
use App\Rx;
use App\User;
use App\Role;
use App\Services\ImageService;

class ApiController extends Controller
{

	protected $imgService;

	public function __construct(ImageService $img){
		$this->imgService = $img;
	}

	

public function regPatient(Request $request){


    $data=$request->all();

    //return $data;
    //return $imagebase;
    $file_name = 'upload_'. rand().'.jpg';
    $folder_path= public_path('images/') . $file_name;
    //return $data[0];
    // $patient_code=$data[0]->patient_code;

    // $patient_code=$data[0]['patient_code'];
    // return $patient_code;
     	
     $imagebase=$data[0]['photo'];

     $data[0]['photo'] = $file_name;

     //return $imagebase;
    
    $this->base64ToJpeg($imagebase,$folder_path);
   // $photo_change = $this->base64_to_jpeg($data[0]['photo'], public_path('storage/temp'));
    // return $photo_change;
    // $data[0]['photo'] = $this->imgService->saveImage($request->file('photo'), 'patients/profile');
    $patientRegistered = patientregistration::create($data[0]);
    if($patientRegistered){
    	return response()->json([
	    	'message' => 'Great, the data is stored!!!',
	    	'data' => $patientRegistered
	    ]);
    }else{
    return response()->json([
	    	'message' => 'Sorry, the data could is not stored!!!',
	    	'data' => 'not saved'
	    ]);
}



}

public function base64ToJpeg($base64_string, $output_file) {
    // open the output file for writing
    $ifp = fopen( $output_file, 'c+' ); 

    // split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode( ',', $base64_string );

   // dd($data);

    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $data[0] ) );

    // clean up the file resource
    fclose( $ifp ); 

    return $output_file; 
}

}
