<?php

namespace App\Http\Controllers;

use App\Pathology;
use App\flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PathologyController extends Controller
{
    public function store(Request $request){
        $pathology = $request->all();
        $pathology['test_id'] = implode(',', $request->test_id);
        $pathology['result'] = implode(',', $request->result);
        Pathology::create($pathology);
        // $this->storingSession();
        return redirect('/home#pathology');
    }}
