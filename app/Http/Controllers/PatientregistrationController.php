<?php

namespace App\Http\Controllers;

use App\patientregistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Gender;
use App\Doctor;
use App\Opd;
use App\ChiefComplaints;
use App\FollowUp;
use App\HistoryIllness;
use App\Investigation;
use App\OnExamination;
use App\Pathology;
use App\Rx;
use App\User;
use App\Role;
use App\Department;
use App\TestCategory;
use App\Test;
use App\SystematicExams;

class PatientregistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genders = Gender::all();
       return  view('backend.patientregistration.create',compact('genders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\patientregistration  $patientregistration
     * @return \Illuminate\Http\Response
     */
    public function show(patientregistration $patientregistration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\patientregistration  $patientregistration
     * @return \Illuminate\Http\Response
     */
    public function edit(patientregistration $patientregistration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\patientregistration  $patientregistration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, patientregistration $patientregistration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\patientregistration  $patientregistration
     * @return \Illuminate\Http\Response
     */
    public function destroy(patientregistration $patientregistration)
    {
        //
    }

    /**
     * Get the patient by Id
     */
    
    public function getById(Request $request){
        $id = $request->patient_id;
        $patient = patientregistration::where('patient_code', $id)->first();
        $patient = patientregistration::findOrFail($id);
        session()->put('patient', $patient);
        $patients = patientregistration::paginate(5);
        $doctors = Doctor::all();
        $opd = Opd::where('patient_id', session()->get('patient')->id)->get();
        $pathologies = Pathology::where('patient_id', session()->get('patient')->id)->get();
        $rxes = Rx::where('patient_id', session()->get('patient')->id)->get();
        $chiefComplaints = ChiefComplaints::where('patient_id', session()->get('patient')->id)->get();
        $historyIllness = HistoryIllness::where('patient_id', session()->get('patient')->id)->get();

        $investigations = Investigation::where('patient_id', session()->get('patient')->id)->get();        

        $examinations = OnExamination::where('patient_id', session()->get('patient')->id)->orderBy('id', 'DESC')->first();
        $departments = Department::all();
        $test_categories = TestCategory::all();
        $tests = Test::all();
        $systematic_examinations = SystematicExams::where('patient_id', session()->get('patient')->id)->orderBy('id', 'DESC')->first();
        if($examinations)
            $check = true;
        else
            $check = false;
        // dd($check);
        $users = User::all();
        $roles = Role::all();
        return view('dashboard.index', compact('patient', 'patients','doctors', 'chiefComplaints', 'historyIllness', 'investigations', 'examinations','rxes', 'users', 'roles','opd','check','pathologies', 'departments', 'test_categories', 'tests', 'systematic_examinations'));

    }

    public function getPatient(){
        if(session()->get('patient')){
            $patient = session()->get('patient');
        }
        else{
            $patient = patientregistration::first();
            session()->put('patient', $patient);
        }
        $rxes = Rx::where('patient_id', session()->get('patient')->id)->get();
        $opd = Opd::where('patient_id', session()->get('patient')->id)->get();
        $pathologies = Pathology::where('patient_id', session()->get('patient')->id)->get();

        $patients = patientregistration::paginate(5);
        $doctors = Doctor::all();
        $chiefComplaints = ChiefComplaints::where('patient_id', session()->get('patient')->id)->get();
        // dd($chiefComplaints);
        $historyIllness = HistoryIllness::where('patient_id', session()->get('patient')->id)->get();
        $investigations = Investigation::where('patient_id', session()->get('patient')->id)->get();


        $examinations = OnExamination::where('patient_id', session()->get('patient')->id)->orderBy('id', 'DESC')->first();
        $departments = Department::all();
        $test_categories = TestCategory::all();
        $tests = Test::all();
        
        $systematic_examinations = SystematicExams::where('patient_id', session()->get('patient')->id)
                                    ->orderBy('id', 'DESC')->get();
        if($examinations)
            $check = true;
        else
            $check = false;
        // dd($check);
        $users = User::all();
        $roles = Role::all();
        return view('dashboard.index', compact('patient', 'patients','doctors', 'chiefComplaints', 'historyIllness', 'investigations', 'examinations','rxes', 'users', 'roles','opd','check','pathologies', 'departments', 'test_categories', 'tests', 'systematic_examinations'));


    }

    public function opdRegistration(Request $request){
        $opd = Opd::create($request->all());
        return redirect('/home#opd-tab');
    }

    public function userManagement(){ 
        $users = User::all();  
        $roles = Role::all();

        return view('tabs.userManagement', compact('users', 'roles'));
    }
}
