<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HistoryIllness;

class HistoryIllnessController extends Controller
{
    /**
     * Store the chiefComplaints
     */
    
    public function store(Request $request){
    	HistoryIllness::create($request->all());
    	// $this->storingSession();
    	return redirect('/home#history_illness-tab');
    }


    /**
     * Delete the record
     */
    public function destroy($id){
    	$historyIllness = HistoryIllness::find($id);
    	$historyIllness->delete();
    	// $this->storingSession();
    	return redirect('/home#history_illness-tab');
    }

    public function update(Request $request, $id){
        $history = HistoryIllness::findOrFail($id);
        $input = $request->all();
        $history->update($input);
        return redirect('/home#history_illness-tab');
    }
}
