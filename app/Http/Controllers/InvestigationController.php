<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investigation;
use App\Services\ImageService;

class InvestigationController extends Controller
{

	/*
	 * Image service variable
	 */
	protected $imageService;

	/*
	 * Constructor for ImageService Class
	 */
	public function __construct(ImageService $imageService){
		$this->imageService = $imageService;
	}

    /*
     Store
     */
    public function store(Request $request){
    	$data = $request->all();
    	$data['investigation_report'] = $this->imageService->saveImage($request->file('investigation_report'), 'images/reports');
    	Investigation::create($data);
    	return redirect('/home#investigation-tab');
    }

    /*
    Delete function by Id
     */
    public function destroy($id){
    	$data = Investigation::findOrFail($id); 
    	$path = public_path() . '/images/' . $data->investigation_report;
    	if(file_exists($path)){
	        @unlink($path);
	    }
    	$data->delete();
    	return redirect('/home#investigation-tab');
    }

    public function update(Request $request, $id){
        $investigation = Investigation::findorFail($id);
        $input = $request->all();
        if($request->hasFile('investigation_report')){
            $input['investigation_report'] = $this->imageService->saveImage($request->file('investigation_report'), 'images/reports');
        } else {
            unset($input['investigation_report']);
        }
        $investigation->update($input);
        return redirect('/home#investigation-tab');

    }
}