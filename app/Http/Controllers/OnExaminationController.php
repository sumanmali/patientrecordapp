<?php

namespace App\Http\Controllers;

use App\SystematicExams;
use Illuminate\Http\Request;
use App\OnExamination;

class OnExaminationController extends Controller
{
    /**
     * Store the on Examination
     */
    
    public function store(Request $request){
        OnExamination::create($request->all());
    	// $this->storingSession();
    	return redirect('/home#oe');
    }


    /**
     * Delete the record
     */
    public function destroy($id){
    	$historyIllness = OnExamination::find($id);
    	$historyIllness->delete();
    	// $this->storingSession();
    	return redirect('/home#oe');
    }

    /**
     * update the record
     */
    public function update(Request $request, $id){
        $history = OnExamination::findOrFail($id);
        $input = $request->all();
        $history->update($input);
        return redirect('/home#oe');
    }
    
    public function oeexaminationn(Request $request){
        SystematicExams::create($request->all()); 
             
        return redirect('/home#oe');
    }
}
