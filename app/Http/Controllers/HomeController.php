<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Pathology;
use App\SystematicExams;
use App\Rx;
use App\patientregistration;
use App\TestCategory;
use App\Test;
use App\Department;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * [registered description]
     * @return [type] [description]
     */
    public function registered(){
        return view('registered');
    }

    /**
     * [printPescription description]
     * @param  [type] $patient_id [description]
     * @return [type]             [description]
     */
    public function printPescription($patient_id){
        $data = Rx::where('patient_id', session()->get('patient')->id)->get();
        $patient = patientregistration::where('id', session()->get('patient')->id)->orderBy('id', 'DESC')->first();
        return view('template.printTemplate', compact('data', 'patient'));
    }
    /**
     * [exportPdf description]
     * @return [type] [description]
     */
    public function exportPdf(){
        $pdf = PDF::loadView('template.printTemplate');
        return $pdf->stream('prescription.pdf',  array("Attachment" => false));
    }

    /**
     * [printReceipt description]
     * @return [type] [description]
     */
    public function printReceipt(){
        $patient = patientregistration::where('id', session()->get('patient')->id)->orderBy('id', 'DESC')->first();
        return view('template.receipt', compact('patient'));
    }

    public function printReport(){
        $patient = patientregistration::where('id', session()->get('patient')->id)->orderBy('id', 'DESC')->first();
        $pathologies = Pathology::where('patient_id', session()->get('patient')->id)->orderBy('id', 'DESC')->get();
        $examinations = SystematicExams::where('patient_id', session()->get('patient')->id)->orderBy('id', 'DESC')->get();
        $test_categories = TestCategory::all();
        $tests = Test::all();
        $departments = Department::all();
        if($pathologies)
            $check = true;
        else
            $check = false;
        return view ('template.testReport', compact('patient', 'departments', 'pathologies','examinations','check', 'test_categories', 'tests'));
    }
}