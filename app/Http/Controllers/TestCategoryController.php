<?php

namespace App\Http\Controllers;

use App\TestCategory;
use App\Department;
use Illuminate\Http\Request;

class TestCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testcat = TestCategory::latest()->get();
        $departments = Department::latest()->get();
        return view('test.test-category.index', compact('testcat','departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::latest()->get();
        return view('test.test-category.create',compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        TestCategory::create($request->all());
        return redirect('/test-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TestCategory  $testCategory
     * @return \Illuminate\Http\Response
     */
    public function show(TestCategory $testCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TestCategory  $testCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(TestCategory $testCategory,$id)
    {
        $testcat = TestCategory::findOrFail($id);
        $departments = Department::latest()->get();
        return view('test.test-category.edit', compact('testcat','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TestCategory  $testCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TestCategory $testCategory,$id)
    {
        $testcat  = TestCategory::findOrFail($id);
        $input = $request->all();
        $testcat->fill($input)->save();
        return redirect('/test-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TestCategory  $testCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(TestCategory $testCategory,$id)
    {
        $testcat  = TestCategory::findOrFail($id)->delete();
        return redirect('/test-category');
    }
}
