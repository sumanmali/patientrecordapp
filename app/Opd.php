<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opd extends Model
{
    protected $fillable = [
    	'patient_id','patient_type','doc_id','checkup_date','admission_date','discharge_date',
    ];
}
