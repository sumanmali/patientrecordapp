<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryIllness extends Model
{
    protected $fillable = [
    	'patient_id', 'problem', 'answer', 'user_id'
    ];
}
