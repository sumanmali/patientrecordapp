<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investigation extends Model
{
    protected $fillable = [
    	'user_id', 'patient_id', 'result', 'remark', 'investigation_report', 'investigation_name'
    ];
}
