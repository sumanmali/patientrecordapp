<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class patientregistration extends Model
{
    protected $fillable = ['patient_code', 'first_name', 'sur_name', 'DOB', 'age', 'gender', 'join_date', 'permanent_address', 'home_number', 'mobile_no', 'birthplace', 'occupation', 'brought_by', 'brought_phone_no', 'patient_email', 'temp_address', 'emergency_no', 'office_address', 'chronic_diseases', 'office_number', 'photo'];
}
