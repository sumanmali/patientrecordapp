<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rx extends Model
{
  protected $fillable = [
    	'patient_id', 'med_name', 'duration', 'using_time', 'advice', 'treatment_stage', 'user_id'
    ];
}
