<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystematicExams extends Model
{
    protected $fillable = [
        'patient_id', 'user_id', 'test', 'sub_test'
    ];
}
