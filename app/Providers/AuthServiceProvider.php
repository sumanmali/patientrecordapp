<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function ($user){
            return $user->roles['0']->name == 'Admin';
        });

        Gate::define('isReception', function ($user){            
            return $user->roles['0']->name == 'Reception';
        });

        Gate::define('isLabTechnician', function ($user){
            return $user->roles['0']->name == 'Lab Technician';
        });

        Gate::define('isDoctor', function ($user){
            return $user->roles['0']->name == 'Doctor';
        });

        Gate::define('isNurse', function ($user){
            return $user->roles['0']->name == 'Nurse';
        });
    }
}
