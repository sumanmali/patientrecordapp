<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestCategory extends Model
{
    Protected $fillable = [
        'name','dept_id'
    ];
}
