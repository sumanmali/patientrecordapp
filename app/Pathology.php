<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pathology extends Model
{
    Protected $fillable = [
    	'patient_id','department_id','test_id', 'result', 'lab_ref', 'collected_on', 'reported_on', 'user_id', 'ref_by'
    ];
}