<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnExamination extends Model
{
    protected $fillable = [
    	'patient_id', 'bp', 'temp','pulse','resp','spo2','others','height','weight','bmi','pallor','lympha','clubbing','dehydration','cyanosis','icterus',
    	'oedema','o_others','family_history','past_history','user_id','personal_history',''
    ];
}
