<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowUp extends Model
{
    protected $fillable = [
    	'dctors_name', 'patient_id','follow_up','advice','user_id','signature_photo', 'followup_date'
    ];
}
