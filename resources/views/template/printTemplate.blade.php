<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	

	<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<title>Print Template</title>

	<style>
		body{
			font-size: 16px;
			font-family: Nunito;
		}
		section.print-header {
		    text-align: center;
		    padding: 80px 30px 30px;
		}
		section.patient-detail .table {
		    border: 1px solid #efefef;
		}
		section.patient-detail .table tr td {
		    padding-top: 7px;
		    padding-bottom: 5px;
		}
		.medicineCanvas {
		    height: 600px;
		    border: 1px solid #efefef;
		    position: relative;
		    background: #efefef2e;
		}
		.medicineCanvas img {
		    position: absolute;
		    top: 28%;
		    left: 35%;
		    opacity: 0.2;
		    width: 30%;
		}
		section.medicines {
		    padding-top: 30px;
		}
		footer {
		    padding: 50px 0px;
		    text-align: right;
		}
		footer h6 {
		    padding-right: 27px;
		    padding-top: 10px;
		}
		table.table.medicineList {
		    width: 70%;
		    padding: 50px !important;
		    position: absolute;
		    left: 13%;
		    top: 14%;
		}
		table.table.medicineList {
		    height: 400px;
		    background: white;
		}
	</style>
	<!-- <link rel="stylesheet" href="print-preview.css"> -->


</head>
<body>
	<section class="print-header">
		<div class="container">
			<button class="btn btn-info">Medicine Prescription</button>
			<h3 class="mt-2">XYZ Hospital</h3>
			<p>Thapathali, Kathmandu
				<br>
				Near to Ekta Books
				<br>
				Phone: +977 9876543210, 5432167
				<br>
				Email: xyzhospital@gmail.com
			</p>
		</div>
	</section>

	<section class="patient-detail">
		<div class="container">
			<table class="table">
				<tbody>
					<tr>
						<td colspan="3">PESCRIBED BY: <strong>Dr. Suman Mali</strong></td>
					</tr>
					<tr>
						<td colspan="3">PATIENT'S NAME: <strong>{{ $patient->first_name }} {{ $patient->sur_name}}</strong></td>
					</tr>
					<tr>
						<td colspan="3">PATIENT'S ADDRESS: <strong>{{ $patient->temp_address }}</strong></td>
					</tr>
					<tr>
						<td>AGE: <strong>{{ $patient->age }}</strong></td>
						<td>SEX: <strong>{{ $patient->gender }}</strong></td>
						<td>DATE: <strong>{{ date('Y-m-d') }}</strong></td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>

	<section class="medicines">
		<div class="container">
			<div class="medicineCanvas">
				<table class="table medicineList">
					<thead>
						<th>S.N</th>
						<th>Medicine Name</th>
						<th>Treatment Stage</th>
						<th>Dose</th>
						<th>Duration</th>
						<th>Advice</th>
					</thead>
					<tbody>
						@foreach($data as $d)
						<tr>
							<td>{{ $loop->iteration}}</td>
							<td>{{ $d->med_name }}</td>
							<td>{{ $d->treatment_stage}}</td>
							<td>{{ $d->using_time }}</td>
							<td>{{ $d->duration }}</td>
							<td>{{ $d->advice }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<img src="{{ asset('images/logo.png') }}" alt="logo">
			</div>
			<!-- <a href="/exportPdf">Print</a> -->
			
		</div>
	</section>
	<footer>
		<div class="container">
			<img src="{{ asset('images/signature.jpg') }}" alt="Signature">
			<h6>Doctor's Signature</h6>
		</div>
	</footer>
	<div class="container">
	<a class="btn btn-info " style=" float: right;" onclick="print(this)">Print</a>
		
	</div>
	<script src="//code.jquery.com/jquery.min.js"></script>
	<script>

		function print(this){
			this.printPreview()
		}
	</script>
</body>
</html>