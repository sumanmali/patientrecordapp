<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	

	<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/ashish.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<title>Print Report</title>

	<style>
		body{
			font-size: 14px;
			font-family: Nunito;
		}
		section.print-header {
		    text-align: center;
		    padding: 60px 30px 30px;
		}
		section.patient-detail .table {
		    border: 1px solid #efefef;
		}
		section.patient-detail .table tr td {
		    padding-top: 7px;
		    padding-bottom: 5px;
		}
		.medicineCanvas {
		    height: 600px;
		    border: 1px solid #efefef;
		    position: relative;
		    background: #efefef2e;
		}
		.medicineCanvas img {
		    position: absolute;
		    top: 28%;
		    left: 35%;
		    opacity: 0.2;
		    width: 30%;
		}
		section.medicines {
		    padding-top: 30px;
		}
		footer {
		    padding: 50px 0px;
		    text-align: right;
		}
		footer h6 {
		    padding-right: 27px;
		    padding-top: 10px;
		}
		table.table.medicineList {
		    width: 70%;
		    padding: 50px !important;
		    position: absolute;
		    left: 13%;
		    top: 14%;
		}
		table.table.medicineList {
		    height: 400px;
		    background: white;
		}
        .right{
            margin-top: 50px;
            padding-right:100px;
        }
        .container.overallReports {
            border: 1px solid #afafaf;
            padding: 40px;
        }
	</style>
	<!-- <link rel="stylesheet" href="print-preview.css"> -->


</head>
<body>
	<section class="print-header">
		<div class="container">
			<h3 class="mt-2">XYZ Hospital</h3>
			<p>Thapathali, Kathmandu
				<br>
				Near to Ekta Books
				<br>
				Phone: +977 9876543210, 5432167
				<br>
				Email: xyzhospital@gmail.com
			</p>
		</div>
	</section>
	
	<section class="patient-detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<p>Patient Id : <strong>{{ $patient->id }}</strong></p>
					<p>Name : <strong>{{ $patient->first_name }} {{ $patient->sur_name}}</strong></p>
					<p>Age / Gender : <strong>{{ $patient->age }} / {{ $patient->gender }}</strong></p>
					<p>Phone : <strong>{{ $patient->mobile_no }}</strong></p>
					<p>Address : <strong>{{ $patient->temp_address }}</strong></p>
				</div>
				<div class="col-sm-6 right_report_side">
					<div class="make_right">
					<p>Requisition Date : <strong>{{ $patient->reported_on }}</strong></p>
					<p>Served By : <strong>{{ Auth()->user()->name }}</strong></p>				
					<p>Referral : <strong>{{ $patient->temp_address }}</strong></p>
					<p>Referral Hospital : <strong>{{ $patient->temp_address }}</strong></p>
					<p>Collected Date : <strong>{{ date('Y-m-d') }}</strong></p>
					<p>Sample Number : <strong>{{ $patient->temp_address }}</strong></p>
					</div>
				</div>
			</div>
		</div>
    </section>
    <br>
    <section>
        <h4 class="text-center">Reports of <strong>{{ $patient->first_name }}</strong></h4>
        <div class="container overallReports">
            <div class="row">
                <div class="col-sm-4">
                    @if($on_examination)
                        <h6 class="text-center"><strong>Basic Examinations</strong></h6>
                        <table class="table table-bordered vertical-align">
                            <tr>
                                <td>Blood Pressure</td>
                                <td>{{ $on_examination->bp }}</td>
                            </tr>
                            <tr>
                                <td>Temperature</td>
                                <td>{{ $on_examination->temp }}</td>
                            </tr>
                            <tr>
                                <td>Pulse</td>
                                <td>{{ $on_examination->pulse }}</td>
                            </tr>
                            <tr>
                                <td>Respiration</td>
                                <td>{{ $on_examination->resp }}</td>
                            </tr>
                            <tr>
                                <td>SPO2</td>
                                <td>{{ $on_examination->spo2 }}</td>
                            </tr>
                            <tr>
                                <td>Height</td>
                                <td>{{ $on_examination->height }}</td>
                            </tr>
                            <tr>
                                <td>Weight</td>
                                <td>{{ $on_examination->weight }}</td>
                            </tr>
                            <tr>
                                <td>BMI</td>
                                <td>{{ $on_examination->bmi }}</td>
                            </tr>
                        </table>
                        @else
                            <script>
                                alert('One of the table will be missing.');
                            </script>
                        @endif
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <h6 class="text-center"><strong>History Illness</strong></h6>
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                        <td>S.N</td>
                                        <td>History Illness</td>
                                        <td>Status</td>
                                    </thead>
                                    <tbody>
                                        @foreach($history_illness as $history)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $history->problem }}</td>
                                                <td>{{ $history->answer }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <h6 class="text-center"><strong>Pathology</strong></h6>
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                        <td>S.N</td>
                                        <td>Chief Complains</td>
                                        <td>Status</td>
                                    </thead>
                                    <tbody>
                                        @foreach($chief_complaints as $co)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $co->problem }}</td>
                                                <td>{{ $co->answer }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-12">
                                <h6 class="text-center"><strong>Patients Complaint</strong></h6>
                                <table class="table table-bordered">
                                    <thead>
                                        <th>S.N</th>
                                        <th>Test Name</th>
                                        <th>Result</th>
                                        <th>Reference</th>
                                        <th>Lab Reference</th>
                                        <th>Collected On</th>
                                    </thead>
                                    <tbody>
                                        @foreach($pathology as $path)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    @foreach($tests as $test)
                                                        @if($test->id == $path->test_id)
                                                            {{ $test->name }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>{{ $path->result }}</td>
                                                <td>
                                                    @foreach($tests as $test)
                                                        @if($test->id == $path->test_id)
                                                            {{ $test->ref_val }}
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>{{ $path->lab_ref }}</td>
                                                <td>{{ $path->collected_on }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-sm-8">
                    <h6 class="text-center"><strong>Medicine</strong></h6>
                    <table class="table table-bordered">
                        <thead>
                            <th>S.N</th>
                            <th>Medicine Name</th>
                            <th>Duration</th>
                            <th>Using Time</th>
                            <th>Advice</th>
                            <th>Treatment Stage</th>
                        </thead>
                        <tbody>
                            @foreach($rxes as $med)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $med->med_name }}</td>
                                    <td>{{ $med->duration }}</td>
                                    <td>{{ $med->using_time }}</td>
                                    <td>{{ $med->advice }}</td>
                                    <td>{{ $med->treatment_stage }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-4">
                    <h6 class="text-center"><strong>Follow Up</strong></h6>
                    <table class="table table-bordered">
                        <thead>
                            <th>Doctor's Name</th>
                            <th>Followup Date</th>
                        </thead>
                        <tbody>
                            @foreach($follow_ups as $follow)
                            <tr>
                                <td>{{ $follow->dctors_name }}</td>
                                <td>{{ $follow->followup_date }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="footer">
		<p style="text-align: center;">"END OF REPORT"</p>
		<div class="right">
            <p style="text-align: center;float:right;">----------------
			<br>Ashish Dulal<br>
			Senior Med Lab Technological<br>
			NHPC Reg. No "A"-1404
			</p>
        </div>	
    </section>
</body>
</html>