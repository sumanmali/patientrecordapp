<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	

	<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/ashish.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<title>Print Report</title>

	<style>
		body{
			font-size: 16px;
			font-family: Nunito;
		}
		section.print-header {
		    text-align: center;
		    padding: 80px 30px 30px;
		}
		section.patient-detail .table {
		    border: 1px solid #efefef;
		}
		section.patient-detail .table tr td {
		    padding-top: 7px;
		    padding-bottom: 5px;
		}
		.medicineCanvas {
		    height: 600px;
		    border: 1px solid #efefef;
		    position: relative;
		    background: #efefef2e;
		}
		.medicineCanvas img {
		    position: absolute;
		    top: 28%;
		    left: 35%;
		    opacity: 0.2;
		    width: 30%;
		}
		section.medicines {
		    padding-top: 30px;
		}
		footer {
		    padding: 50px 0px;
		    text-align: right;
		}
		footer h6 {
		    padding-right: 27px;
		    padding-top: 10px;
		}
		table.table.medicineList {
		    width: 70%;
		    padding: 50px !important;
		    position: absolute;
		    left: 13%;
		    top: 14%;
		}
		table.table.medicineList {
		    height: 400px;
		    background: white;
		}
	</style>
	<!-- <link rel="stylesheet" href="print-preview.css"> -->


</head>
<body>
	<section class="print-header">
		<div class="container">
			<button class="btn btn-info">Report Receipt</button>
			<h3 class="mt-2">XYZ Hospital</h3>
			<p>Thapathali, Kathmandu
				<br>
				Near to Ekta Books
				<br>
				Phone: +977 9876543210, 5432167
				<br>
				Email: xyzhospital@gmail.com
			</p>
		</div>
	</section>
	
	<section class="patient-detail">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<p>Patient Id : <strong>{{ $patient->id }}</strong></p>
					<p>Name : <strong>{{ $patient->first_name }} {{ $patient->sur_name}}</strong></p>
					<p>Age / Gender : <strong>{{ $patient->age }} / {{ $patient->gender }}</strong></p>
					<p>Phone : <strong>{{ $patient->mobile_no }}</strong></p>
					<p>Address : <strong>{{ $patient->temp_address }}</strong></p>
				</div>
				<div class="col-sm-6 right_report_side">
					<div class="make_right">
					<p>Requisition Date : <strong>{{ $patient->reported_on }}</strong></p>
					<p>Served By : <strong>{{ Auth()->user()->name }}</strong></p>				
					<p>Referral : <strong>{{ $patient->temp_address }}</strong></p>
					<p>Referral Hospital : <strong>{{ $patient->temp_address }}</strong></p>
					<p>Collected Date : <strong>{{ date('Y-m-d') }}</strong></p>
					<p>Sample Number : <strong>{{ $patient->temp_address }}</strong></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="medicines">
		<div class="container">
			<div class="medicineCanvas">
				<table class="table medicineList">
					<thead>
						<th>Test Name</th>
						<th>Result</th>
						<th>Unit</th>
						<th>Reference Range</th>
						<!-- <th>Method</th> -->
					</thead>
					<tbody>
					@if($check)
					@foreach($pathologies as $pathology)
					<tr>
						<td>
							<strong>
							<?php
								foreach($departments as $department){
									if($pathology->department_id == $department->id){
										echo $department->name;
									}
								}
							?>
							</strong>
						</td>
					</tr>
					<tr>
						<td height="10" style="border-top: none; font-weight: bold">
							@foreach($test_categories as $test_cat)
								@if($test_cat->id == $pathology->test_cat)
									{{ $test_cat->name }}
								@endif
							@endforeach
						</td>
					</tr>
					<tr>
						<td>
							@foreach($tests as $test)
								<?php
									$testsConverted = explode(',', $pathology->test_id);
									foreach($testsConverted as $testConv){
										if($test->id == $pathology->test_id){
											echo $test->name ;
											echo '<br/>';
										}
									}
								?>
							@endforeach
						</td>
						<td>
						<?php
							$results = explode(',', $pathology->result);
							foreach($results as $result){
								echo $result;
								echo '<br/>';
							}
						?>
						</td>
						<td>
						@foreach($tests as $test)
							<?php
								$testsConverted = explode(',', $pathology->test_id);
								foreach($testsConverted as $testConv){
									if($test->id == $pathology->test_id){
										echo $test->unit ;
										echo '<br/>';
									}
								}
							?>
						@endforeach
						</td>
						<td>
						@foreach($tests as $test)
							<?php
								$testsConverted = explode(',', $pathology->test_id);
								foreach($testsConverted as $testConv){
									if($test->id == $pathology->test_id){
										echo $test->ref_val ;
										echo '<br/>';
									}
								}
							?>
						@endforeach
						</td>
						<!-- <td></td> -->
					</tr>
						@endforeach
						@else
						<tr>
							<td><strong>No data available</strong></td>
						</tr>
					@endif
					</tbody>
				</table>
				<img src="{{ asset('images/logo.png') }}" alt="logo">
			</div>
			<!-- <a href="/exportPdf">Print</a> -->
			<br>
			<!-- <p>***H=High | L=Low***</p> -->
			<p>Reported On:<strong> {{ date('Y-m-d | h:m:s') }}</strong></p>
			<p style="text-align: center;">"END OF REPORT"</p>
			<p style="text-align: center;float:right;">----------------
			<br>Ashish Dulal<br>
			Senior Med Lab Technological<br>
			NHPC Reg. No "A"-1404
			</p>
		</div>
	</section>
	<!-- <div class="container">
	<a class="btn btn-info " style=" float: right;" onclick="print(this)">Print</a>
	</div> -->
		<!-- <footer>
			
		</footer>
		<script src="//code.jquery.com/jquery.min.js"></script>
		<script>

			function print(this){
				this.printPreview()
			}
		</script> -->
</body>
</html>