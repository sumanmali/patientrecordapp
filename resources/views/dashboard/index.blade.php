@extends('layouts.app')

@section('content')
<script src="{{ asset('js/keyboardAccess.js') }}">
</script>
<div class="container">
	<section class="mainPage">
		<ul class="nav nav-tabs main-tab" role="tablist">
			<li class="nav-item" onclick="tryfunc('patientList');">
				<a href="#selection" class="nav-link active show" id="selection-tab" role="tab" data-toggle="tab" aria-controls="selection" aria-selected="true">Selection<br>( <span class="small-box">Alt</span> + S)</a>
			</li>
			<li class="nav-item">
				<a href="#opd" class="nav-link" id="opd-tab"
				 role="tab" data-toggle="tab" aria-controls="opd">OPD<br>( <span class="small-box">Alt</span> + O)</a>
			</li>
			<li class="nav-item" onclick="tryfunc('coTable')">
				<a href="#c_o" class="nav-link" id="c_o-tab" role="tab" data-toggle="tab" aria-controls="c_o">C/O<br>( <span class="small-box">Alt</span> + C)</a>
			</li>
			<li class="nav-item" onclick="tryfunc('historyTable')">
				<a href="#history_illness" class="nav-link" id="history_illness-tab" role="tab" data-toggle="tab" aria-controls="history_illness">History Illness<br>( <span class="small-box">Alt</span> + H)</a>
			</li>
			<li class="nav-item" onclick="tryfunc('opTable')">
				<a href="#oe" class="nav-link" id="oe-tab" role="tab" data-toggle="tab" aria-controls="oe">OE<br>( <span class="small-box">Alt</span> + N)</a>
			</li>
			<li class="nav-item" onclick="tryfunc('pathologyTable')">
				<a href="#pathology" class="nav-link" id="pathology-tab" role="tab" data-toggle="tab" aria-controls="pathology">Pathology<br>( <span class="small-box">Alt</span> + A)</a>
			</li>
			<li class="nav-item" onclick="tryfunc('investigationTable')">
				<a href="#investigation" class="nav-link" id="investigation-tab" role="tab" data-toggle="tab" aria-controls="investigation">Investigation<br>( <span class="small-box">Alt</span> + I)</a>
			</li>
			<li class="nav-item" onclick="tryfunc('rxTable')">
				<a href="#rx" class="nav-link" id="rx-tab" role="tab" data-toggle="tab" aria-controls="rx">Rx<br>( <span class="small-box">Alt</span> + R)</a>
			</li>
			<li class="nav-item">
				<a href="#follow_up" class="nav-link" id="follow_up-tab" role="tab" data-toggle="tab" aria-controls="follow_up">Follow Up<br>( <span class="small-box">Alt</span> + L)</a>
			</li>
			<li class="nav-item">
				<a href="#report" class="nav-link" id="report-tab" role="tab" data-toggle="tab" aria-controls="report">Report<br>( <span class="small-box">Alt</span> + T)</a>
			</li>
			
		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade show active" id="selection" aria-labelledby="selection-tab">
				@include('tabs.selection')
			</div>
			<div role="tabpanel" class="tab-pane fade" id="opd" aria-labelledby="opd-tab">
				@include('tabs.opd')
			</div>
			<div role="tabpanel" class="tab-pane fade" id="c_o" aria-labelledby="c_o-tab">
				@include('tabs.c_o')
			</div>
			<div role="tabpanel" class="tab-pane fade" id="history_illness" aria-labelledby="history_illness-tab">
				@include('tabs.history')
			</div>
			<div role="tabpanel" class="tab-pane fade" id="oe" aria-labelledby="oe-tab">
				@include('tabs.oe')
				
			</div>
			<div role="tabpanel" class="tab-pane fade" id="pathology" aria-labelledby="pathology-tab">
				@include('tabs.pathology')
				
			</div>
			<div role="tabpanel" class="tab-pane fade" id="investigation" aria-labelledby="investigation-tab">
				@include('tabs.investigation')	
			</div>
			<div role="tabpanel" class="tab-pane fade" id="rx" aria-labelledby="rx-tab">
				@include('tabs.rx')	
			</div>
			<div role="tabpanel" class="tab-pane fade" id="follow_up" aria-labelledby="follow_up-tab">
				@include('tabs.follow')	
			</div>
			<div role="tabpanel" class="tab-pane fade" id="report" aria-labelledby="report-tab">
				@include('tabs.report')	
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="{{ asset('js/keyboardAccess.js') }}"></script>
@endsection