@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="doctors">
	<div class="c_o_content">
		<div class="header-text">
			<h3>
				Department's List
			</h3>
			<a href="/department/create" class="link-create"><i class="fa fa-plus"></i></a>
		</div>
	</div>

	<table class="table table-borderless table-data3">
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Encharge</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($departments as $department)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{$department->name}}</td>
				<td>{{$department->encharge}}</td>
				<td><a href="{{route('department.edit', $department->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
					<form method="post" action="{{route('department.delete',$department->id)}}">
						@csrf
						{{ method_field('DELETE') }}
						<button type="submit" onclick="confirmation(event)" class="btn btn-danger">Delete</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>


@endsection

@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>
@endsection