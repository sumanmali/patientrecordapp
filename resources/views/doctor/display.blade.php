@extends('layouts.app')

@section('content')
<div class="doctors">
	<div class="c_o_content">
		<div class="header-text">
			<h3>
				Doctor's List
			</h3>
			<a href="{{ route('doctors.create')}}" class="link-create"><i class="fa fa-plus"></i></a>
		</div>
	</div>
	<table class="table table-hover">
		<thead>
			<th>S.N</th>
			<th>Name</th>
			<th>Specialization</th>
			<th>Department</th>
			<th>Action</th>
		</thead>
		<tbody>
			@foreach($doctors as $doctor)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $doctor->doctor_name }}</td>
				<td>{{ $doctor->specialization }}</td>
				<td>
					@foreach($departments as $department)
					@if($doctor->dept_id == $department->id)
						{{ $department->name }}
					@endif
					@endforeach
				</td>
				<td>
					<a href="{{ route('doctors.edit', [$doctor->id]) }}" class="btn btn-primary btn-block">Edit</a>
					<form action="{{ route('doctors.destroy', [$doctor->id]) }}" method="POST">
						@csrf
						@method('delete')
						<button class="btn btn-danger btn-block" onclick="confirmation(event)">Delete</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection
@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>

@endsection