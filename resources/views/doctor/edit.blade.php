@extends('layouts.app')

@section('content')

<div class="doctors">
	<form action="{{ route('doctors.update', $doctor->id) }}" method="POST">
		@csrf
		@method('PUT')
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label for="doctor_name">Name</label>
					<input type="text" class="form-control" name="doctor_name" value="{{ $doctor->doctor_name }}">
				</div>
				<div class="form-group">
					<label for="specialization">Specialization</label>
					<input type="text" class="form-control" name="specialization" value="{{ $doctor->specialization }}">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="dept_id">Department</label>
					<select name="dept_id" id="" class="form-control">
						<option value="">Select Department</option>
						@foreach($departments as $department)
							<option value="{{ $department->id }}" <?php if($department->id == $doctor->dept_id) echo 'selected' ;?>>{{ $department->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<button class="btn btn-primary btn-block submit-doc" type="submit">Submit</button>
				</div>
			</div>
		</div>	
	</form>
</div>

@endsection