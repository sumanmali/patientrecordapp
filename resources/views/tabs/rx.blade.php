@if(Auth()->user()->hasAnyRole(['Doctor', 'Admin']))
<div class="c_o_content">
	<div class="header-text">
		<h3>
			Rx
		</h3>
	</div>
</div>
<section>
	<div class="container-fluid">
		<div class="nextButton">
			<a onclick="toFollowUp(event)" class="upperCut fa fa-chevron-right"></a>
			<script>
				function toFollowUp(e){
					e.preventDefault();
					window.location.href="home#follow_up-tab";
					location.reload();
				}
			</script>
		</div>
		<div class="compliants">
			<div class="inner-header">
				<h5>
					Enter Medicine
				</h5>
			</div>
			<form action="/rxentry" method="POST" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="patient_id" value="{{ $patient->id }}">
				<input type="hidden" required="" name="user_id" value="{{ Auth()->user()->id }}">
				<div class="row">
					<div class="col-sm-6">
						<div class="patient_no">
							<label for="medicine name" class="patient_no_label col-sm-4">Medicine Name</label>
							<input type="text" required="" class="col-sm-7" name="med_name">
						</div>
						<div class="patient_no">
							<label for="use_time" class="patient_no_label col-sm-4">Using Time</label>
							<span class="use_time_backgrd col-sm-7">
								<input type="radio" value="od" name="using_time" > OD &nbsp;
								<input type="radio" value="bd" name="using_time"> BD &nbsp;
								<input type="radio" value="tds" name="using_time"> TDS &nbsp;
								<input type="radio" value="qid" name="using_time"> QID &nbsp;
							</span>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="patient_no">
							<label for="medicine duration" class="patient_med_dur col-sm-4">Duration</label>
							<input type="text" value="" required="" class="col-sm-7" name="duration">
						</div>
						<div class="patient_no">
							<label for="medicine_advice" class="patient_no_label col-sm-4">Advice</label>
							<input type="text" value="" required="" class="col-sm-7" name="advice">
						</div>
					</div>
				</div>	
				<div style="margin-top: 35px;" class="row">
					<div class="col-sm-7">
						<div class="displayComplaints">
							<div class="inner-header">
								<h5>Enter Treatment Stage</h5>
							</div>
							<form>
								<div class="patient_no">
									<label for="treatment stage name" class="patient_no_label col-sm-4">Treatment Stage</label>
									<input type="text" class="col-sm-7" name="treatment_stage">
								</div>
					
							</form>
						</div>
					</div>
					<div class="col-sm-5 rx_btn">
						<button type="submit">Add</button>
						<a href="/prescription/print/{{ $patient->id }}" target="_blank" class="btn btn-info">Print Prescription</a>
					</div>

				</div>					
			</form>			
		</div>
		<div class="clear"></div>
		<div class="displayComplaints">
			<div class="inner-header">
				<h5>Existing Medicine</h5>
			</div>
			<table class="table table-hover dataShow" id="rxTable">
				<thead>
					<td>Id</td>
					<th>Advice</th>
					<th>medicine name</th>
					<th>use time</th>
					<th>duration</th>
					<th>Treatment Stage</th>
					<th></th>
				</thead>
				<tbody>
					@foreach($rxes as $rxes)
					<tr>
						<td>{{ $rxes->id }}</td>
						<td>{{ $rxes->advice }}</td>
						<td>{{ $rxes->med_name }}</td>
						<td>{{ $rxes->using_time }}</td>
						<td>{{ $rxes->duration }}</td>
						<td>{{ $rxes->treatment_stage }}</td>
						@can('isAdmin')
						<td>
							<button type="button" class="btn btn-info btn-block mb-1" data-toggle="modal" data-target="#editForm{{ $rxes->id }}" style="display: none;">Edit</button>
							<div class="modal fade" id="editForm{{ $rxes->id }}" tabindex="-1" role="dialog" aria-labelledby="editFormTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
									<form action="/rxentry/edit/{{ $rxes->id }}" method="POST">
										@csrf
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle">Edit Record</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<input type="hidden" name="patient_id" value="{{ $patient->id }}">
												<input type="hidden" required="" name="user_id" value="{{ Auth()->user()->id }}">
												<div class="row">
													<div class="col-sm-6">
														<div class="patient_no">
															<label for="medicine name" class="patient_no_label col-sm-4">Medicine Name</label>
															<input type="text" required="" class="col-sm-7" value="{{ $rxes->med_name }}" name="med_name">
														</div>
														<div class="patient_no">
															<label for="use_time" class="patient_no_label col-sm-4">Using Time</label>
															<span style="padding:0;" class="use_time_backgrd col-sm-7">
																<input type="radio" value="od" name="using_time" {{ $rxes->using_time == "od" ? 'checked' : '' }}> OD &nbsp;
																<input type="radio" value="bd" name="using_time" {{ $rxes->using_time == "bd" ? 'checked' : '' }}> BD &nbsp;
																<input type="radio" value="tds" name="using_time" {{ $rxes->using_time == "tds" ? 'checked' : '' }}> TDS &nbsp;
																<input type="radio" value="qid" name="using_time" {{ $rxes->using_time == "qid" ? 'checked' : '' }}> QID &nbsp;
															</span>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="patient_no">
															<label for="medicine duration" class="patient_med_dur col-sm-4">Duration</label>
															<input type="text" value="{{ $rxes->duration }}" required="" class="col-sm-7" name="duration">
														</div>
														<div class="patient_no">
															<label for="medicine_advice" class="patient_no_label col-sm-4">Advice</label>
															<input type="text" value="{{ $rxes->advice }}" required="" class="col-sm-7" name="advice">
														</div>
													</div>
												</div>	
												<div style="margin-top: 35px;" class="row">
													<div class="col-sm-7">
														<div class="displayComplaints">
															<div class="inner-header">
																<h5>Enter Treatment Stage</h5>
															</div>
															<form>
																<div class="patient_no">
																	<label for="treatment stage name" class="patient_no_label col-sm-4">Treatment Stage</label>
																	<input type="text" class="col-sm-7" value="{{ $rxes->treatment_stage }}" name="treatment_stage">
																</div>
															</form>
														</div>
													</div>
													
												</div>				
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-primary">Submit Changes</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							


							<form action="/rxentry/delete/{{ $rxes->id }}" method="post" >
								@csrf
								{{ method_field('delete') }}
								<button type="submit" class="btn btn-danger btn-block" style="display: none" id="deleteDataBtn{{ $rxes->id }}" onclick="confirmation(event)">Delete</button>
							</form>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>

@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>
@endsection
@else
<div class="container noAccess">
	<p class="para-noaccess">You dont have correct access for this!</p>
</div>
@endif