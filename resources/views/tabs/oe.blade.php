@if(Auth()->user()->hasAnyRole(['Doctor', 'Admin', 'Nurse']))
<div class="container">
	<div class="history_content">
		<div class="header-text">
			<h3>
				On-Examination
			</h3>
		</div>
		<div class="nextButton">
				<a onclick="toPathology(event)" class="fa fa-chevron-right"></a>
				<script>
					function toPathology(e){
						e.preventDefault();
						window.location.href="home#pathology-tab";
						location.reload();
					}
				</script>
			</div>
		<div class="row">
			<div class="col-sm-3">
				<div class="vitals compliants">
					<div class="inner-header">
						<h5>
							Vitals
						</h5>
					</div>
					<form action="/oesubmit" method="POST" class="col-sm-12 form-details">
						@csrf
						<input type="hidden" name="patient_id" value="{{ $patient->id }}">
						<input type="hidden" required="" name="user_id" value="{{ Auth()->user()->id }}">
						<div class="form-group">
							<label for="bp" class="col-sm-4">B/P</label>
							<input type="text" name="bp" value="@if($check){{$examinations->bp}}@endif" class="col-sm-5" >
							<span class="col-sm-1 unit">mmhg</span>
						</div>
						<div class="form-group">
							<label for="temp" class="col-sm-4">Temp</label>
							<input type="text" name="temp" value="@if($check){{$examinations->temp}}@endif" class="col-sm-5" >
							<span class="col-sm-1 unit">F</span>
						</div>
						<div class="form-group">
							<label for="pulse_rate" class="col-sm-4">Pulse</label>
							<input type="text" name="pulse" value="@if($check){{$examinations->pulse}}@endif" class="col-sm-5" >
							<span class="col-sm-1 unit">/m</span>
						</div>
						<div class="form-group">
							<label for="respiration" class="col-sm-4">Resp</label>
							<input type="text" name="resp" value="@if($check){{$examinations->resp}}@endif" class="col-sm-5">
							<span class="col-sm-1 unit">/m</span>
						</div>
						<div class="form-group">
							<label for="spo2" class="col-sm-4">SPO2</label>
							<input type="text" name="spo2" value="@if($check){{$examinations->spo2}}@endif" class="col-sm-5">
							<span class="col-sm-1 unit">%</span>
						</div>
						<div class="form-group">
							<label for="others" class="col-sm-4">Others</label>
							<input type="text" name="others" value="@if($check){{$examinations->others}}@endif" class="col-sm-5" >
						</div>
						<div class="form-group">
							<label for="height" class="col-sm-4">Height</label>
							<input type="text" name="height" value="@if($check){{$examinations->height}}@endif" class="col-sm-5" >
							<span class="col-sm-1 unit">cm</span>
						</div>
						<div class="form-group">
							<label for="weight" class="col-sm-4">Weight</label>
							<input type="text" name="weight" value="@if($check){{$examinations->weight}}@endif" class="col-sm-5">
							<span class="col-sm-1 unit">kg</span>
						</div>
						<div class="form-group">
							<label for="bmi" class="col-sm-4">BMI</label>
							<input type="text" name="bmi" value="@if($check){{$examinations->bmi}}@endif" class="col-sm-5">
						</div>
						<div class="form-group">
							<button class="btn btn-block" type="submit">Save</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="others compliants">
					<div class="inner-header">
						<h5>
							Others
						</h5>
					</div>
					<form action="" class="col-sm-12 form-details">
						<div class="form-group">
							<label for="pallor" class="col-sm-6">Pallor</label>
							<select name="pallor" id="" class="col-sm-5">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<div class="form-group">
							<label for="lympha" class="col-sm-6">Lympha</label>
							<select name="lympha" id="" class="col-sm-5">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<div class="form-group">
							<label for="clubbing" class="col-sm-6">Clubbing</label>
							<select name="clubbing" id="" class="col-sm-5">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<div class="form-group">
							<label for="dehydration" class="col-sm-6">Dehydration</label>
							<select name="dehydration" id="" class="col-sm-5">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<div class="form-group">
							<label for="cyanosis" class="col-sm-6">Cyanosis</label>
							<select name="cyanosis" id="" class="col-sm-5">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<div class="form-group">
							<label for="icterus" class="col-sm-6">Icterus</label>
							<select name="icterus" id="" class="col-sm-5">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<div class="form-group">
							<label for="oedema" class="col-sm-6">Oedema</label>
							<select name="oedema" id="" class="col-sm-5">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
						<div class="form-group">
							<label for="others_o" class="col-sm-6">Others</label>
							<select name="others_o" id="" class="col-sm-5">
								<option value="0">No</option>
								<option value="1">Yes</option>
							</select>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="sys_examination compliants">
					<div class="inner-header">
						<h5>
							Systematic Examination
						</h5>
					</div>
					<form action="/oeexamination" class="form-details row" method='POST'>
						@csrf
						<div class="col-sm-10">
							<div class="col-sm-12">
								<input type="hidden" name="patient_id" value="{{ $patient->id }}">
								<input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">	
								<label for="test" class="col-sm-5">Test Category</label>
								<select name="test" id="mainCat" class="col-sm-6" onChange="setTestCat()">
									<option value="">Select Test Category</option>
									@foreach($test_categories as $testcat)
										<option value="{{ $testcat->id }}">{{ $testcat->name }}</option>
									@endforeach
								</select>
								<script>
										function setTestCat(){
											var test_cat = document.getElementById('mainCat').value;
											sendTestCat(test_cat);
										}
									</script>
								<label for="sub_test" class="col-sm-5">Sub Test</label>
								<select name="sub_test" id="subCat">
									<option value="">Select Subtest</option>
									<script>
										function sendTestCat(test_cat){
												var catSelect = document.getElementById("subCat");
												for(var i = catSelect.options.length - 1; i >0 ; i--){
													if(i > 0){
														catSelect.remove(i);
													}
												}
												var loop = '<?php echo json_encode($tests) ?>';
												loop = JSON.parse(loop);
												loop.forEach(function(data){
													
													var testCat = data.cat_id;
													if(testCat == test_cat){
														var select = document.getElementById('subCat');
														var option = document.createElement('option');
														option.value = data.id;
														option.innerHTML = data.name;
														select.appendChild(option);
													}
												});													
											}
									</script>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<button type="submit"><i class="fa fa-plus"></i>Add</button>
						</div>
					</form>
					<div class="compliants sys-exam">
						<p class="header-text">Systematic Examination</p>
						<div class="sys_exam">
						@if($systematic_examinations)
							@foreach($systematic_examinations as $sys_exam)
								@foreach($test_categories as $test_cat)
									@if($test_cat->id == $sys_exam['test'])
										<h6>{{ $test_cat->name }}</h6>
									@endif
								@endforeach
								<ol>
									@foreach($tests as $test)
										@if($test->id == $sys_exam['sub_test'])
											<li>{{ $test->name }}</li>
										@endif
									@endforeach
								</ol>	
							@endforeach
						@endif							
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="compliants past-history">
					<div class="row">
						<div class="col-sm-4">
							<div class="complaints hist">
								<div class="inner-header">
									<h5>Family History</h5>
								</div>
								<textarea name="hypertension" value="hypertension" rows="6">Hypertension
								</textarea>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="complaints hist">
								<div class="inner-header">
									<h5>Past History</h5>
								</div>
								<textarea name="past-history" value="past-history" rows="6">Diabetes
								</textarea>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="complaints hist">
								<div class="inner-header">
									<h5>Personal History</h5>
								</div>
								<textarea name="personal-history" value="personal-history" rows="6">Chewing Tobacco
								</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@else
		@include('permission.nopermission')
		@endif