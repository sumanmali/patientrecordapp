@if(Auth()->user()->hasAnyRole(['Doctor', 'Admin']))
<div class="c_o_content">
	<div class="header-text">
		<h3>
			Follow Up
		</h3>
	</div>
</div>
<section>
	<div class="container-fluid">
		<div class="nextButton">
				<a onclick="toReport(event)" class="upperCut fa fa-chevron-right"></a>
				<script>
					function toReport(e){
						e.preventDefault();
						window.location.href="home#report-tab";
						location.reload();
					}
				</script>
			</div>
		<div class="displayComplaints col-sm-12">
			<div class="inner-header">
				<h5>Enter ur follow up</h5>
				@if($errors->any())
				@foreach($errors->all() as $error)
				<ul>
					<li>{{$error}}</li>
				</ul>
				@endforeach
				@endif
			</div>
			<form action="/followup" method="POST" enctype="multipart/form-data">
				<div class="row">
					@csrf
					<input type="hidden" name="patient_id" value="{{ $patient->id }}">
					<input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
					<div class="col-sm-6">
						<div class="compliants">
							<div class="inner-header">
								<h5>
									Follow Up
								</h5>
								
							</div>
							<div class="complaints">
								<textarea rows="10" cols="78" name="follow_up"></textarea>
							</div>

							<div class="complaints">
								<div class="patient_no">
									<label for="followup_date" class="patient_no_label col-sm-4">Followup Date</label>
									<input type="date" value="Saturday March 02,2018" class="col-sm-7" name="followup_date">
								</div>
							</div>		
						</div>
					</div>
					<div class="col-sm-6">
						<div class="compliants">
							<div class="inner-header">
								<h5>
									Advice
								</h5>
								
							</div>
							<div class="complaints">
								<textarea rows="10" cols="78" name="advice"></textarea>
							</div>

							<div class="complaints">
								<div class="patient_no">
									<label for="dctors_name" class="patient_no_label col-sm-3">Doctor Name</label>
									<input type="text" value=" Dr. Bashumati" class="col-sm-7" name="dctors_name">


								</div>
							</div>		
						</div>
					</div>
					<div class="col-sm-6"></div>
					<div class="col-sm-6">
						<div class="signature_section">
							<div class="sign_part">
								<img src="{{asset('/images/signature.jpg')}}" alt="your signature">
								<input type="file" id="signature" accept="image/png, image/jpg, image/jpeg, video/mp4" name="signature_photo" class="form-control-file">
							</div>
						</div>
					</div>
				</div>
				<span style="float: right;"><button type="submit">Submit</button></span>

			</form>
		</div>
	</div>
</section>
@else
@include('permission.nopermission')
@endif