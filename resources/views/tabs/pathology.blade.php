@if(Auth()->user()->hasAnyRole(['Lab Technician', 'Admin']))

<div class="c_o_content">
	<div class="header-text">
		<h3>
			Pathology
		</h3>
	</div>
</div>
<section>
	<div class="container-fluid">
	<div class="nextButton">
			<a onclick="toInvest(event)" class="upperCut fa fa-chevron-right"></a>
			<script>
				function toInvest(e){
					e.preventDefault();
					window.location.href="home#investigation-tab";
					location.reload();
				}
			</script>
		</div>
		<div class="col-sm-12">
			<form action="/pathology" method="post" enctype="multipart/form-data">
			@csrf
				<div class="row">
					<div class="col-sm-6">
						<div class="compliants">
							<div class="inner-header">
								<h5>
									Enter Pathology
								</h5>
							</div>
							<input type="hidden" name="patient_id" value="{{ $patient->id }}">
							<input type="hidden" required="" name="user_id" value="{{ Auth()->user()->id }}">
							<!-- <div class="complaints">

									<input type="hidden" name="patient_id" value="{{ $patient->id }}">
									<input type="hidden" required="" name="user_id" value="{{ Auth()->user()->id }}">
									<div class="patient_no">
										<label for="department_id" class="pathology_dept col-sm-4">Department</label>
										<select class="col-sm-6" name="department_id" id="dept_select" onchange="setDepartment()">
											<option value="">Select Department</option>
											@foreach($departments as $department)
												<option value="{{ $department->id }}" >{{ $department->name }}</option>
											@endforeach
										</select>
										<script>
											function setDepartment(){
												var dept = document.getElementById('dept_select').value;
												sendValue(dept);
											}
										</script>
									</div>
									<div class="patient_no">
										<label for="test_cat" class="pathology_test col-sm-4">Test Category</label>
										<select class="col-sm-7" name="test_cat" id="cat_select" oninput="setCategory()">
											<option value="">Select Test Category</option>
											<script>
												function sendValue(dept){
													var catSelect = document.getElementById("cat_select");
													for(var i = catSelect.options.length - 1; i >0 ; i--){
														if(i > 0){
															catSelect.remove(i);
														}
													}
													var loop = '<?php echo json_encode($test_categories) ?>';
													loop = JSON.parse(loop);
													loop.forEach(function(data){
														var testCat = data.dept_id;
														if(testCat == dept){
															var select = document.getElementById('cat_select');
															var option = document.createElement('option');
															option.value = data.id;
															option.innerHTML = data.name;
															select.appendChild(option);
														}
													});
												}

												function setCategory(){
													var cat = document.getElementById('cat_select').value;
													sendCatValue(cat);
												}
											</script>
										</select>
									</div>
									<div class="patient_no">
										<label for="test_id" class="pathology_test col-sm-4">Test Name</label>
										<select class="col-sm-7" name="test_id" id="test_select">
											<option value="">Select Test Category</option>
											<script>
												function sendCatValue(param){

													var testSelect = document.getElementById("test_select");
													for(var i = testSelect.options.length - 1; i >0 ; i--){
														if(i > 0){
															testSelect.remove(i);
														}
													}
													var test = '<?php echo json_encode($tests) ?>';
													test = JSON.parse(test);
													test.forEach(function(data){
														var temp = data.cat_id;
														if(temp == param){
															var select = document.getElementById('test_select');
															var option = document.createElement('option');
															option.value = data.id;
															option.innerHTML = data.name;
															select.appendChild(option);
														}
													});
												}
											</script>
										</select>
									</div>
									<div class="patient_no">
										<label for="result" class="pathology_dept col-sm-4">Result</label>
										<input type="text" value="" class="col-sm-7" name="result">
									</div>
							</div> -->
							<div class="complaints">
								<div class="patient_no">
									<label for="department_id" class="pathology_dept col-sm-4">Department</label>
									<select class="col-sm-6" name="department_id" id="dept_select" onchange="setDepartment()">
										<option value="">Select Department</option>
										@foreach($departments as $department)
											<option value="{{ $department->id }}" >{{ $department->name }}</option>
										@endforeach
									</select>
									<script>
										function setDepartment(){
											var dept = document.getElementById('dept_select').value;
											sessionStorage.removeItem('dept_id');
											sessionStorage.setItem('dept_id', dept);
											sendValue(dept); 
										}
									</script>	
								</div>
								<div class="patient_no">
									<table class="table" id="testRecord">
										<thead>
											<th>Test Name</th>
											<th>Result</th>
										</thead>
										<tbody>
											<tr>
												<td>
													<select name="test_id[]" id="test_select" class="form-control">
														<option value="">Select Test</option>
														<script>
															function sendValue(param){
																var testSelect = document.getElementById("test_select");
																for(var i = testSelect.options.length - 1; i > 0 ; i--){
																	if(i > 0){
																		testSelect.remove(i);
																	}
																}
																var test_cats = '<?php echo json_encode($test_categories)?>';
																test_cats = JSON.parse(test_cats);

																test_cats.forEach(function(cat){
																	var dept_id = cat.dept_id;
																	if(dept_id == param){
																		var test = '<?php echo json_encode($tests) ?>';
																		test = JSON.parse(test);
																		test.forEach(function(data){
																			var temp = cat.id;
																			if(temp == data.cat_id ){
																				var select = document.getElementById('test_select');
																				var option = document.createElement('option');
																				option.value = data.id;
																				option.innerHTML = data.name;
																				select.appendChild(option);
																			}
																		});
																	}
																});
															}
														</script>
													</select>
												</td>
												<td>
													<input type="text" value="" class="form-control" name="result[]">
												</td>
											</tr>	
										</tbody>
									</table>
									<script>
										function buttonClicked(){
											jQuery.noConflict();
											$('#testRecord').append(`
											<tr>
												<td>
													<select class="form-control selectTestChild" 
														name="test_id[]">
														<option value="">Select Test</option>
													</select>
												</td>
												<td>
													<input type="text" value="" class="form-control" name="result[]">
												</td>
											</tr>
											`);
											
											$('.selectTestChild').one("click", function (event){
												// console.log('Inside first');
												JSON.parse('<?php echo json_encode($test_categories)?>')
													.forEach(function (cat){
														// console.log('Inside first foreach');
														if(sessionStorage.getItem('dept_id') == cat.dept_id){
															JSON.parse('<?php echo json_encode($tests) ?>')
																.forEach(function (testData){
																	// console.log('Inside second foreach');
																	if(cat.id == testData.cat_id){
																		// console.log(testData);
																		var option = document.createElement('option');
																		option.value = testData.id;
																		option.innerHTML = testData.name;
																		event.target.appendChild(option);
																	}
																})
														}
												})
											});	
										}
									</script>
									<button type="button" 
											class="btn btn-par btn-block"  
											onclick="buttonClicked()" 
											id="addButton">Add</button>								
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6"> 
						<div class="compliants">
							<div class="inner-header">
								<h5>
									Printing Of Pathology Report Parameters
								</h5>
							</div>

							<div class="complaints">
									<div class="patient_no">
										<label for="lab_ref" class="pathology_ref col-sm-4">Lab Ref.</label>
										<input type="number" min="0" value="" class="col-sm-7" name="lab_ref">
									</div>
									<div class="patient_no">
										<label for="collected_on" class="pathology_collected col-sm-4">Collected On</label>
										<input type="date" value="" class="col-sm-7" name="collected_on">
									</div>
									<div class="patient_no">
										<label for="reported_on" class="pathology_report col-sm-4">Reported On</label>
										<input type="date" value="" class="col-sm-7" name="reported_on">
									</div>
									<div class="patient_no">
										<label for="reference_by" class="pathology_dept col-sm-4">Ref. By.</label>
										<input type="text" value="" class="col-sm-7" name="ref_by">
									</div>
							</div>
							<div style="margin: 10px 0px 0px;display: inline;" class="col-sm-5 rx_btn">
							<input type="submit" class="btn btn-primary" value="Submit">
							<a href="/pathology/print/{{ $patient->id }}"
								target="_blank"
								class="btn btn-info" style="float:right">Print Pathology Report</a>
						</div>
						</div>

					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="displayComplaints col-sm-12">
				<div class="inner-header">
					<h5>Exiting Pathology</h5>
				</div>
				<table class="table table-hover" id="pathologyTable">
					<thead>
						<th>Department Name</th>
						<th>Test Category</th>
						<th>Test Name</th>
						<th>Result</th>
						<th>Reference</th>
						<th>Reported On</th>
						<!-- <th>Option</th> -->
					</thead>
					<tbody>
						@foreach($pathologies as $path)
							<tr>
								<td>
									@foreach($departments as $department)
										@if($department->id == $path->department_id)
											{{ $department->name }}
										@endif
									@endforeach
								</td>
								<td>
									@foreach($test_categories as $testcat)
										@if($testcat->id == $path->test_cat)
											{{ $testcat->name }}
										@endif
									@endforeach
								</td>
								<td>
									@foreach($tests as $test)
										<?php
											$testsConverted = explode(',', $path->test_id);
											foreach($testsConverted as $testConv){
												if($test->id == $path->test_id){
													echo $test->name ;
													echo '<br/>';
												}
											}
										?>
									@endforeach
								</td>
								<td>
									<?php
									$results = explode(',', $path->result);
									foreach($results as $result){
										echo $result;
										echo '<br/>';
									}
									?>
								</td>
								<td>
									@foreach($tests as $test)
										<?php
											$testsConverted = explode(',', $path->test_id);
											foreach($testsConverted as $testConv){
												if($test->id == $path->test_id){
													echo $test->ref_val ;
													echo '<br/>';
												}
											}
										?>
									@endforeach
								</td>
								<td>{{$path->reported_on}}</td>
								<!-- Trigger the modal with a button -->
								<td>
								<!-- <button type="button" data-toggle="modal" data-target="#myPathology" class="btn btn-primary">Report</button> -->
								</td>
							</tr>
							@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<!-- Modal -->
<div class="modal fade" id="myPathology" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h2><strong>Pathology Report Receipt</strong></h2>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">

				<!-- <link rel="stylesheet" href="print-preview.css"> -->
				<section style="padding-top: 0px;" class="print-header">
					<div class="container">
						<h3 class="mt-2">Norvic Hospital</h3>
						<p>Thapathali, Kathmandu
							<br>
							Near to Ekta Books
							<br>
							Phone: +977 9876543210, 5432167
							<br>
							Email: xyzhospital@gmail.com
						</p>
					</div>
				</section>
				<section class="patient-detail">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<p>Patient Id : <strong>{{ $patient->id }}</strong></p>
								<p>Name : <strong>{{ $patient->first_name }} {{ $patient->sur_name}}</strong></p>
								<p>Age / Gender : <strong>{{ $patient->age }} / {{ $patient->gender }}</strong></p>
								<p>Phone : <strong>{{ $patient->mobile_no }}</strong></p>
								<p>Address : <strong>{{ $patient->temp_address }}</strong></p>
							</div>
							<div class="col-sm-6 right_report_side">
								<div class="make_right">
								<p>Requisition Date : <strong>{{ $patient->reported_on }}</strong></p>
								<p>Served By : <strong>{{ Auth()->user()->name }}</strong></p>
								<p>Referral : <strong>{{ $patient->temp_address }}</strong></p>
								<p>Referral Hospital : <strong>{{ $patient->temp_address }}</strong></p>
								<p>Collected Date : <strong>{{ date('Y-m-d') }}</strong></p>
								<p>Sample Number : <strong>{{ $patient->temp_address }}</strong></p>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="medicines">
					<div class="container">
								<p style="text-transform: uppercase; font-size: 20px;margin-bottom: 5px;"><strong>@if($check){{ $examinations->test }}@endif</strong></p>
						<div class="medicineCanvas">
							<table class="table medicineList">
								<thead>
									<th>Test Name</th>
									<th>Result</th>
									<th>Unit</th>
									<th>Reference Range</th>
									<th>Method</th>
								</thead>
								<tbody>

									<tr>
										<td>
											<div class="sys_exam">
											@if($check)
											<h3>{{$examinations->test}}</h3>
											@endif
											<div class="examination-test-title" >
												@if($check)
												<?php
												$data = "$examinations->sub_test";
												$splittedstring = explode(",", $data);
												foreach ($splittedstring as $key => $value) {
													echo "-" . $value . "<br>";
												}

												?>
													@endif
												</div>
											</div>
										</td>
										<td>5000</td>
										<td>%</td>
										<td>20-40</td>
										<td>Specedometry</td>
									</tr>
								</tbody>
							</table>
							<img src="{{ asset('images/logo.png') }}" alt="logo">

						</div>
						<!-- <a href="/exportPdf">Print</a> -->
						<br>
						<p>***H=High | L=Low***</p>
						<p>Reported On:<strong> {{ date('Y-m-d | h:m:s') }}</strong></p>
						<p style="text-align: center;">"END OF REPORT"</p>
						<p style="text-align: center;float:right;">----------------<br>Ashish Dulal<br>Senior Med Lab Technological<br>NHPC Reg. No "A"-1404</p>
					</div>
				</section>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@else
@include('permission.nopermission')
@endif