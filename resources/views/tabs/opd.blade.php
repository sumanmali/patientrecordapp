<div class="c_o_content">
	<div class="header-text">
		<h3>
			OPD information
		</h3>
	</div>
</div>
<section>
	<div class="container-fluid">
		<div class="nextButton">
			<a onclick="toCO(event)" class="fa fa-chevron-right"></a>
			<script>
				function toCO(e){
					e.preventDefault();
					window.location.href="home#c_o-tab";
					location.reload();
				}
			</script>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="compliants">
					<div class="inner-header">
						<h5>
							Selected patient
						</h5>
					</div>
					<div class="row">
						<div class="col-sm-8">
							<form action="{{ url('/getPatient') }}#opd" method="post" id="idSubmit">
								@csrf
								<div class="patient_no">
									<label for="patient_id" class="patient_no_label col-sm-4">Patient no.</label>
									<input type="text" value="{{ $patient->id }}" class="col-sm-7" onchange="submitForm()" name="patient_id">
								</div>
							</form>
							<form action="">
								<div class="patient_no">
									<label for="name" class="patient_no_label col-sm-4">Name</label>
									<input type="text" value="{{ $patient->first_name  }} {{ $patient->sur_name }}" class="col-sm-7" name="patient_name">
								</div>
								<div class="patient_no">
									<label for="age" class="patient_no_label col-sm-4">Age</label>
									<input type="text" value="{{ $patient->age }}" class="col-sm-7" name="patient_age">
								</div>
								<div class="patient_no">
									<label for="gender" class="patient_no_label col-sm-4">Gender</label>
									<select name="gender" id="">
										@if($patient->value == "male")
										<option value="male">Male</option>
										<option value="female">Female</option>

										@else
										<option value="female">Female</option>
										<option value="male">Male</option>

										@endif
									</select>
								</div>
								<div class="patient_no">
									<label for="dob" class="patient_no_label col-sm-4">DOB</label>
									<input type="text" value="{{ $patient->DOB}}" class="col-sm-7" name="patient_DOB">
								</div>
							</form>
						</div>						
						<div class="col-sm-4">
							<div class="selected_patient_image">
								<img src="" alt="selected patient photo">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-4">
				<div class="compliants">
					<div class="inner-header">
						<h5>
							OPD Registration
						</h5>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<form action="/newOpdRegistration" method="POST">
								<input type="hidden" name="patient_id" value="{{ $patient->id }}">
								@csrf
								<div class="patient_no">
									<label for="patient_type" class="patient_no_label col-sm-4">OPD Type</label>
									<select name="patient_type" class="col-sm-7" required>
										@foreach($opd as $opds )
									@if($patient->id == $opds->patient_id )
										<option>old</option>
										<option>new</option>

										@elseif($patient->id !== $opds->patient_id )
										<option>new</option>
										<option>old</option>

										@endif
										@endforeach
										

									</select>
								</div>
								<div class="patient_no">
									<label for="doc_id" class="patient_no_label col-sm-4">Doctor Name</label>
									<select name="doc_id" class="col-sm-7" required>

										<!-- <option value="">---</option> -->
										@foreach($doctors as $doc)
										<option value="{{$doc->id}}">{{$doc->doctor_name}}</option>
										@endforeach

									</select>
								</div>
								<div class="patient_no">
									<label for="checkup_date" class="patient_no_label col-sm-4">checkup date</label>
									<input type="date" class="col-sm-7" value="<?php echo date("Y-m-d"); ?>" name="checkup_date">
								</div>
								<div class="patient_no">
									<label for="admission_date" class="patient_no_label col-sm-4">Admission Date</label>
									<input type="date" class="col-sm-7" value="<?php echo date("Y-m-d"); ?>" name="admission_date">
									<input type="checkbox" name="admitted">
								</div>
								<div class="patient_no">
									<label for="discharge_date" class="patient_no_label col-sm-4">Discharge Date</label>
									<input type="date" class="col-sm-7" value="<?php echo date("Y-m-d"); ?>" name="discharge_date">
									<input type="checkbox" name="discharged">
								</div>
								<div style="float: right; padding-right: 25px;" class="button_opd_registration">
									<button type="submit">Update</button>
									<button type="submit">Add</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="displayComplaints">
					<div class="inner-header">
						<h5>Existing Complaints</h5>
					</div>
					<table class="table table-hover dataShow" id="opTable">
						<thead>
							<th>Checkup Date</th>
							<th>Admission Date</th>
							<th>Discharge</th>
							<th>Doctor Name</th>
							<th>Remarks</th>
						</thead>
						<tbody>
							@foreach($opd as $opd)
							<tr>
								<td>{{$opd->checkup_date}}</td>
								<td>{{$opd->admission_date}}</td>
								<td>{{$opd->discharge_date}}</td>
								<td>{{$opd->doctor_name}}</td>
								<!-- Trigger the modal with a button -->
								<td><button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Report</button></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog modal-lg">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<h2>Report Of Someone</h2>
				<table class="table table-hover" id="">
					<thead>
						<th>Checkup Date</th>
						<th>Admission Date</th>
						<th>Discharge</th>
						<th>Doctor Name</th>
						<th>Remarks</th>
					</thead>
					<tbody>
						<tr>
							<td>11/07/2019</td>
							<td>N/A</td>
							<td>N/A</td>
							<td>Dr. Bhanu Pd. Dahal</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
		
	</div>
</div>

