@if(Auth()->user()->hasAnyRole(['Doctor', 'Admin']))

	<div class="c_o_content">
		<div class="header-text">
			<h3>
				Chief Complaints
			</h3>
		</div>
		<div class="nextButton">
		<a onclick="toHistroyIllness(event)" class="fa fa-chevron-right"></a>
		<script>
			function toHistroyIllness(e){
				e.preventDefault();
				window.location.href="home#history_illness-tab";
				location.reload();
			}
		</script>
	</div>
		<div class="compliants col-sm-5">
			<div class="inner-header">
				<h5>
					Enter Complaints
				</h5>
			</div>
			<form action="/cosubmit" method="POST" class="col-sm-12" id="">
				@csrf
				<input type="hidden" name="patient_id" value="{{ $patient->id }}">
				<input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
				<label for="problem" class="col-sm-3">Problem</label>
				<input type="text" class="col-sm-8" name="problem">
				<label for="answer" class="col-sm-3">Answer</label>
				<input type="text" class="col-sm-8" name="answer">
				<button type="submit">Submit</button>
			</form>

		</div>
		<div class="clear"></div>
		<div class="displayComplaints col-sm-8">
			<div class="inner-header">
				<h5>Existing Complaints</h5>
			</div>
			<table class="table table-hover dataShow" id="coTable">
				<thead>
					<th>S.N</th>
					<th>Problem</th>
					<th>Answer</th>
					@can('isAdmin')
					<th></th>
					@endcan
				</thead>
				<tbody>
					@foreach($chiefComplaints as $chief)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $chief->problem }}</td>
						<td>{{ $chief->answer }}</td>
						@can('isAdmin')
						<td>
							<button type="button" class="btn btn-info btn-block mb-1" data-toggle="modal" data-target="#editForm{{ $chief->id }}" style="display: none;">Edit</button>
							<!-- Modal -->
							<div class="modal fade" id="editForm{{ $chief->id }}" tabindex="-1" role="dialog" aria-labelledby="editFormTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<form action="/c_o/edit/{{ $chief->id }}" method="POST">
										@csrf
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle">Edit Record</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<input type="hidden" name="patient_id" value="{{ $patient->id }}">
												<input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
												<label for="problem" class="col-sm-3">Problem</label>
												<input type="text" class="col-sm-8" name="problem" value="{{ $chief->problem }}">
												<label for="answer" class="col-sm-3">Answer</label>
												<input type="text" class="col-sm-8" name="answer" value="{{ $chief->answer }}">
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-primary">Submit Changes</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<form action="/c_o/delete/{{ $chief->id }}" method="post">
								@csrf
								{{ method_field('delete') }}
								<button type="submit" class="btn btn-danger btn-block" style="display: none;" id="coDltBtn{{ $chief->id }}" onclick="confirmation(event)">Delete</button>
							</form>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>
@endsection
@else
@include('permission.nopermission')
@endif