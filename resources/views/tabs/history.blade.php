@if(Auth()->user()->hasAnyRole(['Doctor', 'Admin']))
<div class="container">
	<div class="history_content">
		<div class="header-text">
			<h3>
				History Illness
			</h3>
		</div>
		<div class="nextButton">
				<a onclick="toOE(event)" class="fa fa-chevron-right"></a>
				<script>
					function toOE(e){
						e.preventDefault();
						window.location.href="home#oe-tab";
						location.reload();
					}
				</script>
			</div>
		<div class="compliants history col-sm-5">
			<div class="inner-header">
				<h5>
					Enter History Illness
				</h5>
			</div>
			<form action="/historyIllness" method="POST" class="col-sm-12" id="">
				@csrf
				<input type="hidden" name="patient_id" value="{{ $patient->id }}">
				<input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
				<label for="problem" class="col-sm-3">Problem</label>
				<input type="text" class="col-sm-8" name="problem">
				<label for="answer" class="col-sm-3">Answer</label>
				<input type="text" class="col-sm-8" name="answer">
				<button type="submit">Submit</button>
			</form>
		</div>
		<div class="clear"></div>
		<div class="displayComplaints displayHistory col-sm-8">
			<div class="inner-header">
				<h5>Existing History Illness</h5>
			</div>
			<table class="table table-hover dataShow" id="historyTable">
				<thead>
					<th>S.N</th>
					<th>Problem</th>
					<th>Answer</th>
					@can('isAdmin')

					<th></th>
					@endcan
				</thead>
				<tbody>
					@foreach($historyIllness as $history)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $history->problem }}</td>
						<td>{{ $history->answer }}</td>
						@can('isAdmin')

						<td>
							<button type="button" class="btn btn-info btn-block mb-1"  style="display: none;" data-toggle="modal" data-target="#editHistory{{ $history->id }}">Edit</button>
							<!-- Modal -->
							<div class="modal fade" id="editHistory{{ $history->id }}" tabindex="-1" role="dialog" aria-labelledby="editFormTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<form action="/history/edit/{{ $history->id }}" method="POST">
										@csrf
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle">Edit Record</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<input type="hidden" name="patient_id" value="{{ $patient->id }}">
												<input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
												<label for="problem" class="col-sm-3">Problem</label>
												<input type="text" class="col-sm-8" name="problem" value="{{ $history->problem }}">
												<label for="answer" class="col-sm-3">Answer</label>
												<input type="text" class="col-sm-8" name="answer" value="{{ $history->answer }}">
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-primary">Submit Changes</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<form action="/historyIllness/delete/{{ $history->id }}" method="post">
								@csrf
								{{ method_field('delete') }}
								<button type="submit" class="btn btn-danger btn-block" style="display: none" id="historyDltBtn{{ $history->id }}" onclick="confirmation(event)">Delete</button>
							</form>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>

@endsection
@else
@include('permission.nopermission')
@endif