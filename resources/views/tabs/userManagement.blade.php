@extends('layouts.app')

@section('content')
@can('isAdmin')

<section>
	<div class="user-mgmt">
		 <div class="displayComplaints">
			<div class="inner-header">
				<h5>User Management</h5>
			</div>
			<table class="table table-hover dataShow" id="userManagementTable">
				<thead>
					<th>S.N</th>
					<th>Username</th>
					<th>Email</th>
					<th>Role</th>
					<th></th>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>
							@foreach($roles as $role)
								<a href="/roleChange/{{ $user->id }}/{{ $role->id }}" onclick="confirmPrevilege(event)"
								class="btn @if($user->hasRole($role->name)) btn-selected @else btn-unselected @endif">{{ $role->name }}</a>
							@endforeach
						</td>
						<td>
							<form action="/user/delete/{{ $user->id }}" method="post">
								@csrf
								{{ method_field('delete') }}
								<button type="submit" class="btn btn-danger btn-block" style="display: none;" id="userDeleteBtn{{ $user->id }}" onclick="confirmation(event)">Delete</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>

@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>
@endsection

@else
@include('permission.nopermission')
@endcan

@endsection