@if(Auth()->user()->hasAnyRole(['Doctor', 'Admin']))
<div class="c_o_content">
	<div class="header-text">
		<h3>
			Report Generation
		</h3>
	</div>
</div>
<section>
	<div class="container-fluid">
		<a href="/overallReport" target="_blank" class="btn btn-primary">Generate Overall Report</a>
	</div>
</section>

@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>
@endsection
@else
<div class="container noAccess">
	<p class="para-noaccess">You dont have correct access for this!</p>
</div>
@endif