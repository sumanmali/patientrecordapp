@if(Auth()->user()->hasAnyRole(['Lab Technician', 'Admin']))

<div class="c_o_content">
	<div class="header-text">
		<h3>
			Investigation
		</h3>
	</div>
</div>
<section>
	<div class="container-fluid">
		<div class="nextButton">
				<a onclick="toRX(event)" class="fa fa-chevron-right"></a>
				<script>
					function toRX(e){
						e.preventDefault();
						window.location.href="home#rx-tab";
						location.reload();
					}
				</script>
			</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="compliants">
					<div class="inner-header">
						<h5>
							Enter Investigation
						</h5>
					</div>
					<form action="/investigation" method="POST" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="patient_id" value="{{ $patient->id }}">
						<input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
						<div class="row">
							<div class="col-sm-8">
								<div class="patient_no">
									<label for="investigation_name" class="patient_inv col-sm-4">Inv Name</label>
									<input type="text" class="col-sm-7" name="investigation_name">
								</div>
								<div class="patient_no">
									<label for="result" class="patient_no_label col-sm-4">Result</label>
									<input type="text" class="col-sm-7" name="result">
								</div>
								<div class="patient_no">
									<label for="remark" class="patient_remark col-sm-4">Remark</label>
									<input type="text" class="col-sm-7" name="remark">
									<div class="investigation_btn">
										<!-- <a class="btn btn-info" href="#">/ Select</a> -->
										<button type="submit" class="btn btn-info">Submit</button>
									</div>

								</div>
							</div>	
							<div class="col-sm-4">
								<div class="selected_patient_investigation_image">
									<img src="/images/report.jpg" alt="selected patient photo" id="investigationReport">
								</div>
								<input type="file" accept="image/png, image/jpg, image/jpeg, video/mp4" name="investigation_report" onchange="readUrl(event)">
							</div>
						</div>
					</form>

				</div>
			</div>
			<div class="load_all data">
				<span class="use_time_backgrd col-sm-7">
					<input type="checkbox" value="OD" name="patient_use_time"> Load All Inv. Records
				</span>
			</div>
		</div>

	</div>
</section>
<section class="displayList">
	<div class="container-fluid">
		<div class="row">
			<div class="displayComplaints col-sm-12">
				<table class="table table-hover dataShow" id="investigationTable">
					<thead>
						<th>S No.</th>
						<th>Checkup Date</th>
						<th>Investigation Name</th>
						<th>Result</th>
						<th>Remark</th>
						<th>Invest</th>
						@can('isAdmin')
						<th></th>
						@endcan
					</thead>
					<tbody>
						@foreach($investigations as $investigation)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $investigation->created_at }}</td>
							<td>{{ $investigation->investigation_name }}</td>
							<td>{{ $investigation->result }}</td>
							<td>{{ $investigation->remark}}</td>
							<td class="img_report">
								<img src="{{ asset('storage/images/reports/' . $investigation->investigation_report) }}" alt="{{ $investigation->investigation_name }}" >
								<br>
								<button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#popupImage{{ $investigation->id }}">
									Open Image
								</button>
								<div class="modal fade" id="popupImage{{ $investigation->id }}" tabindex="-1" role="dialog" aria-labelledby="popupImageTitle" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle">{{ $investigation->investigation_name }}</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<img src="{{ asset('storage/images/reports/' . $investigation->investigation_report) }}" alt="{{ $investigation->investigation_name }}">
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</td>
							@can('isAdmin')
							
							<td>
								<button type="button" class="btn btn-info btn-block mb-1" data-toggle="modal" data-target="#editInvestigation{{ $investigation->id }}" style="display: none;">Edit</button>
								<!-- Modal -->
								<div class="modal fade" id="editInvestigation{{ $investigation->id }}" tabindex="-1" role="dialog" aria-labelledby="editFormTitle" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<form action="/investigation/edit/{{ $investigation->id }}" method="POST" enctype="multipart/form-data">
											@csrf
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLongTitle">Edit Record</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<input type="hidden" name="patient_id" value="{{ $patient->id }}">
													<input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
													<div class="row">
														<div class="col-sm-8">
															<div class="patient_no">
																<label for="investigation_name" class="patient_inv col-sm-4">Inv Name</label>
																<input type="text" class="col-sm-7" name="investigation_name" value="{{ $investigation->investigation_name }}">
															</div>
															<div class="patient_no">
																<label for="result" class="patient_no_label col-sm-4">Result</label>
																<input type="text" class="col-sm-7" name="result" value="{{ $investigation->result }}">
															</div>
															<div class="patient_no">
																<label for="remark" class="patient_remark col-sm-4">Remark</label>
																<input type="text" class="col-sm-7" name="remark" value="{{ $investigation->remark }}">
															</div>
														</div>	
														<div class="col-sm-4">
															<div class="selected_patient_investigation_image">
																<img src="{{ asset('storage/images/reports/' . $investigation->investigation_report) }}" alt="selected patient photo" id="investigationReportEdit" >
															</div>
															<input type="file" accept="image/png, image/jpg, image/jpeg, video/mp4" name="investigation_report" onchange="readUrl(event)" value="{{ $investigation->investigation_report }}">
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
													<button type="submit" class="btn btn-primary">Submit Changes</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<form action="/investigation/delete/{{ $investigation->id }}" method="post">
									@csrf
									{{ method_field('delete') }}
									<button type="submit" class="btn btn-danger btn-block" style="display: none;" id="investDltBtn{{ $investigation->id }}" onclick="confirmation(event)">Delete</button>
								</form>
							</td>
							@endcan
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container-fluid">
		<div class="compliants">
			<div class="inner-header">
				<h5>
					Others
				</h5>
			</div>
			<form action="">
				<div class="row">
					<div class="col-sm-6">
						<div class="patient_no">
							<label for="prov_diagnosis" class="patient_no_label col-sm-4">Prov Diagnosis</label>
							<input type="text" class="col-sm-7" name="prov_diagnosis">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="patient_no">
							<label for="final_diagnosis" class="patient_med_dur col-sm-4">Final Diagnosis</label>
							<input type="text" value="" class="col-sm-7" name="final_diagnosis">
						</div>
					</div>
				</div>						
			</form>			
		</div>
	</div>
</section>
@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>

@endsection

@else
@include('permission.nopermission')
@endif