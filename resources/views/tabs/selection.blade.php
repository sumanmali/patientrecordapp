<div class="c_o_content">
	<div class="header-text">
		<h3>
			Patient Selection
		</h3>
	</div>
</div>
<section>
	<div class="container-fluid">
	<div class="nextButton">
		<a onclick="toOPD(event)" class="fa fa-chevron-right"></a>
		<script>
			function toOPD(e){
				e.preventDefault();
				window.location.href="home#opd-tab";
				location.reload();
			}
		</script>
	</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="compliants">
					<div class="inner-header">
						<h5>
							Selected patient
						</h5>
					</div>
					<div class="row">
						<div class="col-sm-8">
							<form action="{{ url('/getPatient') }}" method="post" id="idSubmit">
								@csrf
								<div class="patient_no">
									<label for="patient_id" class="patient_no_label col-sm-4">Patient No.</label>
									<input type="text" value="{{ $patient->id }}" class="col-sm-7" onchange="submitForm()" name="patient_id">
								</div>
							</form>
							<form action="">
								<div class="patient_no">
									<label for="name" class="patient_no_label col-sm-4">Name</label>
									<input type="text" value="{{ $patient->first_name  }} {{ $patient->sur_name }}" class="col-sm-7" name="patient_name">
								</div>
								<div class="patient_no">
									<label for="age" class="patient_no_label col-sm-4">Age</label>
									<input type="text" value="{{ $patient->age }}" class="col-sm-7" name="patient_age">
								</div>
								<div class="patient_no">
									<label for="gender" class="patient_no_label col-sm-4">Gender</label>
									<select name="gender" id="">
										@if($patient->value === "male")
										<option value="male">Male</option>
										<option value="female">Female</option>
										@else
										<option value="female">Female</option>
										<option value="male">Male</option>
										@endif
									</select>
								</div>
								<div class="patient_no">
									<label for="dob" class="patient_no_label col-sm-4">DOB</label>
									<input type="text" value="{{ $patient->DOB}}" class="col-sm-7" name="patient_DOB">
								</div>
							</form>
						</div>						
						<div class="col-sm-4">
							<div class="selected_patient_image">
								<img src="" alt="selected patient photo">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<a href="/receipt/print/{{ $patient->id }}" target="_blank" class="btn btn-info">Print Receipt</a>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container-fluid">
		<div class="row">
		 <div class="displayComplaints col-sm-12">
			<div class="inner-header">
				<h5>Patient List</h5>
			</div>
			
			@if(isset($patients))
			<table class="table table-hover dataShow" id="patientList">
				<thead>
					<th>Patient Code</th>
					<th>FirstName</th>
					<th>Surname</th>
					<th>Date Of Birth</th>
					<th>Age</th>
					<th>Gender</th>
					<th>Join Date</th>
					<th>Permanent Address</th>
					<th>Mobile Number</th>
				</thead>
				<tbody>
					@foreach($patients as $p)
					<tr>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->id}}
								</button>
							</form>
						
						</td>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->first_name }}
								</button>
							</form>
						
						</td>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->sur_name }}
								</button>
							</form>
						
						</td>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->DOB }}
								</button>
							</form>
						
						</td>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->age }}
								</button>
							</form>
						
						</td>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->gender }}
								</button>
							</form>
						
						</td>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->join_date }}
								</button>
							</form>
						
						</td>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->permanent_address }}
								</button>
							</form>
						
						</td>
						<td>
							<form action="/getPatient" method="Post" id="explorePatient{{$p->id}}">
								@csrf()
								<input type="hidden" name="patient_id" value="{{ $p->id }}" >
								<button class="noBorder" 
									onclick="submitForm(explorePatient<?php echo $p->id?>)">
									{{ $p->mobile_no }}
								</button>
							</form>	
						
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{!! $patients->render() !!}
			@endif
		</div>
		</div>
	</div>
</section>

<script>
		function submitForm(){
			document.getElementById('idSubmit').submit();
		}
</script>
