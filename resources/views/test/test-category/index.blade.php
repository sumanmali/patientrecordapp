@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="doctors">
	<div class="c_o_content">
		<div class="header-text">
			<h3>
				Test Category
			</h3>
			<a href="/test-category/create" class="link-create"><i class="fa fa-plus"></i></a>
		</div>
	</div>

	<div class="table-responsive m-b-40">
		@foreach($departments as $department)
		<h3>{{$department->name}}</h3>
		<table class="table table-hover make-gap">

			<thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($testcat as $testcate)
				@if( $department->id === $testcate->dept_id)
				<tr>
					<td>{{ $loop->iteration }}</td>
					<td>{{$testcate->name}}</td>
					<td><a href="{{route('test-category.edit', $testcate->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
						<form method="post" action="{{route('test-category.delete',$testcate->id)}}">
							@csrf
							{{ method_field('DELETE') }}
							<button type="submit" onclick="confirmation(event)" class="btn btn-danger">Delete</button>
						</form>
					</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
		@endforeach
	</div>
</div>

@endsection
@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>
@endsection