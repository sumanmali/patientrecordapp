@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="doctors">

	<form action="/test-category/edit/{{$testcat->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
		@csrf
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="name" class=" form-control-label">Name</label>
			</div>
			<div class="col-12 col-md-9">
				<input type="name" id="name" name="name" value="{{$testcat->name}}" placeholder="Enter The Name..." class="form-control">

			</div>
		</div>
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="encharge" class=" form-control-label">Choose Department</label>
			</div>
			<div class="col-12 col-md-9">
				<select name="dept_id" class="form-control">


					@foreach($departments as $dept)
					@if($dept->id == $testcat->dept_id)
					<option value="{{$dept->id}}">{{$dept->name}}</option>
					@endif
					@endforeach


					@foreach($departments as $dept)
					@if($dept->id != $testcat->dept_id)
					<option value="{{$dept->id}}">{{$dept->name}}</option>
					@endif
					@endforeach
				</select>
			</div>
		</div>
		<button type="submit" class="btn btn-primary btn-sm">
			<i class="fa fa-dot-circle-o"></i> Update
		</button>
	</form>

</div>
@endsection