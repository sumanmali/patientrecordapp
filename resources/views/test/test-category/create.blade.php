@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="doctors">

			<form action="/test-category/create" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="name" class=" form-control-label">Category name</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="text" id="name" name="name" placeholder="Enter test category Name..." class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="department" class=" form-control-label">Choose Departments:</label>
					</div>
					<div class="col-12 col-md-9">
						<select name="dept_id" required class="form-control">

							<option value="">---</option>
							@foreach($departments as $department)

							<option value="{{$department->id}}">{{$department->name}}</option>

							@endforeach
						</select>
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Submit
				</button>
				<button type="reset" class="btn btn-danger btn-sm">
					<i class="fa fa-ban"></i> Reset
				</button>
			</form>
		</div>
</div>
@endsection