@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="doctors">
	
	<form action="/test/create" method="POST" class="form-horizontal" enctype="multipart/form-data">
		@csrf
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="name" class=" form-control-label">Test name</label>
			</div>
			<div class="col-12 col-md-9">
				<input type="text" id="name" name="name" placeholder="Enter test Name..." class="form-control">
				
			</div>
		</div>
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="ref_val" class=" form-control-label">Reference Value</label>
			</div>
			<div class="col-12 col-md-9">
				<input type="text" id="ref_val" name="ref_val" placeholder="Enter Reference Value..." class="form-control">
			</div>
		</div>
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="unit" class=" form-control-label">Test Unit</label>
			</div>
			<div class="col-12 col-md-9">
				<input type="text" id="unit" name="unit" placeholder="Enter Unit..." class="form-control">
				
			</div>
		</div>
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="testcategory" class=" form-control-label">Choose Test Category:</label>
			</div>
			<div class="col-12 col-md-9">
				<select name="cat_id" required class="form-control">

					<option value="">---</option>
					@foreach($testcat as $testcate)

					<option value="{{$testcate->id}}">{{$testcate->name}}</option>

					@endforeach
				</select>
			</div>
		</div>
		<button type="submit" class="btn btn-primary btn-sm">
			<i class="fa fa-dot-circle-o"></i> Submit
		</button>
		<button type="reset" class="btn btn-danger btn-sm">
			<i class="fa fa-ban"></i> Reset
		</button>
	</form>
</div>


@endsection