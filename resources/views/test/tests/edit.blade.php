@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="doctors">
	<form action="/test/edit/{{$tests->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
		@csrf
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="name" class=" form-control-label">Name</label>
			</div>
			<div class="col-12 col-md-9">
				<input type="name" id="name" name="name" value="{{$tests->name}}" placeholder="Enter The Name..." class="form-control">

			</div>
		</div>
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="ref_val" class=" form-control-label">Reference Value</label>
			</div>
			<div class="col-12 col-md-9">
				<input type="text" id="ref_val" name="ref_val" value="{{$tests->ref_val}}" placeholder="Enter Reference Value..." class="form-control">
			</div>
		</div>
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="unit" class=" form-control-label">Test Unit</label>
			</div>
			<div class="col-12 col-md-9">
				<input type="text" id="unit" value="{{$tests->unit}}" name="unit" placeholder="Enter Unit..." class="form-control">

			</div>
		</div>
		<div class="row form-group">
			<div class="col col-md-3">
				<label for="encharge" class=" form-control-label">Choose Test Category</label>
			</div>
			<div class="col-12 col-md-9">
				<select name="cat_id" class="form-control">


					@foreach($testcat as $testcate)
					@if($testcate->id == $tests->cat_id)
					<option value="{{$testcate->id}}">{{$testcate->name}}</option>
					@endif
					@endforeach


					@foreach($testcat as $testcate)
					@if($testcate->id != $tests->cat_id)
					<option value="{{$testcate->id}}">{{$testcate->name}}</option>
					@endif
					@endforeach
				</select>
			</div>
		</div>
		<button type="submit" class="btn btn-primary btn-sm">
			<i class="fa fa-dot-circle-o"></i> Update
		</button>
	</form>
</div>

@endsection