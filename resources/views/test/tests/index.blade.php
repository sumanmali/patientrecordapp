@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="doctors">
	<div class="c_o_content">
		<div class="header-text">
			<h3>
				Test's List
			</h3>
			<a href="/test/create" class="link-create"><i class="fa fa-plus"></i></a>
		</div>
	</div>

	<!-- DATA TABLE-->
	<div class="table-responsive m-b-40">
		@foreach($testcat as $testcat)
		<h3>{{$testcat->name}}</h3>
		<table class="table table-borderless table-data3">
			<thead>
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Reference Value</th>
					<th>Unit</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($tests as $test)
				@if( $testcat->id === $test->cat_id)
				<tr>
					<td>{{ $loop->iteration }}</td>
					<td>{{$test->name}}</td>
					<td>{{$test->ref_val}}</td>
					<td>{{$test->unit}}</td>
					<td><a href="{{route('test.edit', $test->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
						<form method="post" action="{{route('test.delete',$test->id)}}">
							@csrf
							{{ method_field('DELETE') }}
							<button type="submit" onclick="confirmation(event)" class="btn btn-danger">Delete</button>
						</form>
					</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
		@endforeach
	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/sys.js') }}"></script>
@endsection