<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientregistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patientregistrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("patient_code")->nullable();
            $table->string("first_name")->nullable();
            $table->string("sur_name")->nullable();
            $table->text("DOB")->nullable();
            $table->integer("age")->nullable();
            $table->string("gender")->nullable();
            $table->text("join_date")->nullable();
            $table->string("permanent_address")->nullable();
            $table->bigInteger("home_number")->nullable();
            $table->bigInteger("mobile_no")->nullable();
            $table->string("birthplace")->nullable();
            $table->string("occupation")->nullable();
            $table->string("brought_by")->nullable();
            $table->bigInteger("brought_phone_no")->nullable();
            $table->string("patient_email")->nullable();
            $table->string("temp_address")->nullable();
            $table->bigInteger("emergency_no")->nullable();
            $table->string("office_address")->nullable();
            $table->string("chronic_diseases")->nullable();
            $table->bigInteger("office_number")->nullable();
            $table->integer('doc_id')->nullable();
            $table->string('reffered_by')->nullable();
            $table->string('reffered_to')->nullable();
            $table->text('photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patientregistrations');
    }
}
