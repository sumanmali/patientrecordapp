<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnExaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('on_examinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('patient_id')->unsigned();
            $table->string('bp')->nullable();
            $table->double('temp')->nullable();
            $table->integer('pulse')->nullable();
            $table->integer('resp')->nullable();
            $table->integer('spo2')->nullable();
            $table->string('others')->nullable();
            $table->double('height')->nullable();
            $table->double('weight')->nullable();
            $table->double('bmi')->nullable();
            $table->boolean('pallor')->default(false);
            $table->boolean('lympha')->default(false);
            $table->boolean('clubbing')->default(false);
            $table->boolean('dehydration')->default(false);
            $table->boolean('cyanosis')->default(false);
            $table->boolean('icterus')->default(false);
            $table->boolean('oedema')->default(false);
            $table->boolean('o_others')->default(false);
            $table->text('family_history')->nullable();
            $table->text('past_history')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->text('personal_history')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('patient_id')->references('id')->on('patientregistrations')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('on_examinations');
    }
}
