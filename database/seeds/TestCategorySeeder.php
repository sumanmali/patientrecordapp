<?php

use Illuminate\Database\Seeder;
use App\TestCategory;

class TestCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TestCategory::insert([
        	'name' => 'Renal Function Test',
        	'dept_id' => '2'
        ]);

        TestCategory::insert([
        	'name' => 'Complete blood Count(CBC)',
        	'dept_id' => '1'
        ]);

        TestCategory::insert([
        	'name' => 'Differential Count',
        	'dept_id' => '1'
        ]);
    }
}
