<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use App\Doctor;

class doctorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('doctors')->insert([
                 'doctor_name' => $faker->name,
                'specialization' => $faker->word,
                'dept_id' => 1,
            ]);
    }
    }    
}