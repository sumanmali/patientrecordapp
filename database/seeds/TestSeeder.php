<?php

use Illuminate\Database\Seeder;
use App\Test;
class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Test::insert([
        	'name' => 'Sodium',
        	'unit' => 'mmcl/L',
        	'ref_val' => '135.0 - 150.0',
        	'cat_id' => '1'
        ]);

        Test::insert([
        	'name' => 'Potassium',
        	'unit' => 'mmcl/L',
        	'ref_val' => '3.5 - 5.5',
        	'cat_id' => '1'
        ]);

        Test::insert([
        	'name' => 'Total Leucocyte Test',
        	'unit' => '/cumm',
        	'ref_val' => '4000 - 11000',
        	'cat_id' => '2'
        ]);

        Test::insert([
        	'name' => 'Neutrophil',
        	'unit' => '%',
        	'ref_val' => '40 - 70',
        	'cat_id' => '3'
        ]);

        Test::insert([
        	'name' => 'Platelets Count',
        	'unit' => '/cumm',
        	'ref_val' => '150000 - 450000',
        	'cat_id' => '3'
        ]);
    }
}
