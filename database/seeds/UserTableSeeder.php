<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = Role::where('name', 'Admin')->first();
        $labTechnician_role = Role::where('name', 'Lab Technician')->first();
        $doctor_role = Role::where('name', 'Doctor')->first();
        $reception_role = Role::where('name', 'Reception')->first();
        $nurse_role = Role::where('name', 'Nurse')->first();

        $admin = new User();
        $admin->name = 'admin';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('admin123');
        $admin->save();
        $admin->roles()->attach($admin_role);

        $doctor = new User();
        $doctor->name = 'doctor';
        $doctor->email = 'doctor@doctor.com';
        $doctor->password = bcrypt('doctor123');
        $doctor->save();
        $doctor->roles()->attach($doctor_role);

        $labTechnician = new User();
        $labTechnician->name = 'labTechnician';
        $labTechnician->email = 'labTechnician@labTechnician.com';
        $labTechnician->password = bcrypt('labTechnician123');
        $labTechnician->save();
        $labTechnician->roles()->attach($labTechnician_role);

        $reception = new User();
        $reception->name = 'reception';
        $reception->email = 'reception@reception.com';
        $reception->password = bcrypt('reception123');
        $reception->save();
        $reception->roles()->attach($reception_role);

        $nurse = new User();
        $nurse->name = 'nurse';
        $nurse->email = 'nurse@nurse.com';
        $nurse->password = bcrypt('nurse123');
        $nurse->save();
        $nurse->roles()->attach($nurse_role);

//         use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;

// use Faker\Factory as Faker;

// class DatabaseSeeder extends Seeder
// {
//     /**
//      * Run the database seeds.
//      *
//      * @return void
//      */
//     public function run()
//     {
//         $faker = Faker::create();
//         foreach (range(1,10) as $index) {
//             DB::table('users')->insert([
//                 'name' => $faker->name,
//                 'email' => $faker->email,
//                 'password' => bcrypt('secret'),
//             ]);
//     }
//     }    
// }
    }
}