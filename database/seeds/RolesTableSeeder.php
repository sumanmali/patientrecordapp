<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = 'Admin';
        $admin->description = 'A super User';
        $admin->save();

        $doctor = new Role();
        $doctor->name = 'Doctor';
        $doctor->description = 'A doctor User';
        $doctor->save();

        $labTechnician = new Role();
        $labTechnician->name = 'Lab Technician';
        $labTechnician->description = 'A labTechnician User';
        $labTechnician->save();

        $reception = new Role();
        $reception->name = 'Reception';
        $reception->description = 'A reception User';
        $reception->save();

        $nurse = new Role();
        $nurse->name = 'Nurse';
        $nurse->description = 'A nurse User';
        $nurse->save();
    }
}
