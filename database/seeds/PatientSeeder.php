<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('patientregistrations')->insert([
        	'patient_code' => '1',
        	'first_name' => 'nakul',
        	'sur_name' => 'budhathoki',
            'DOB' => '2015-08-20',
            'age' => '15',
            'gender' => 'male',
            'join_date' => '2020-08-26',
            'permanent_address' => 'bhaktapur',
            'home_number' => '20',
            'mobile_no' => '984125252',
            'birthplace' => 'changunarayan',
            'occupation' => 'IT',
            'brought_by' => 'ashish',
            'brought_phone_no' => '984310000',
            'patient_email' => 'nakul@gmail.com',
            'temp_address' => 'none',
            'emergency_no' => '9810121212',
            'office_address' => 'putalisadak',
            'chronic_diseases' => 'fever',
            'office_number' => '014011101',
            'photo' => 'default-thumbnail',

        ]);

        DB::table('patientregistrations')->insert([
        	'patient_code' => '2',
        	'first_name' => 'suresh',
        	'sur_name' => 'hada',
            'DOB' => '2016-08-20',
            'age' => '20',
            'gender' => 'male',
            'join_date' => '2020-08-26',
            'permanent_address' => 'ktm',
            'home_number' => '21',
            'mobile_no' => '984126255',
            'birthplace' => 'changunarayan-park',
            'occupation' => 'mba',
            'brought_by' => 'ashishnepal',
            'brought_phone_no' => '9843101000',
            'patient_email' => 'suresh@gmail.com',
            'temp_address' => 'sukedhara',
            'emergency_no' => '981512112',
            'office_address' => 'putalisadak',
            'chronic_diseases' => 'cough',
            'office_number' => '014011101',
            'photo' => 'default-thumbnail',

        ]);
        DB::table('patientregistrations')->insert([
        	'patient_code' => '3',
        	'first_name' => 'ashish',
        	'sur_name' => 'dulal',
            'DOB' => '2016-08-20',
            'age' => '20',
            'gender' => 'female',
            'join_date' => '2020-08-26',
            'permanent_address' => 'ktm',
            'home_number' => '21',
            'mobile_no' => '9841268525',
            'birthplace' => 'balkot',
            'occupation' => 'MIT',
            'brought_by' => 'ashishnepal',
            'brought_phone_no' => '984310007',
            'patient_email' => 'ashish@gmail.com',
            'temp_address' => 'sukedhara',
            'emergency_no' => '981512112',
            'office_address' => 'kathmandu',
            'chronic_diseases' => 'cancer',
            'office_number' => '014011101',
            'photo' => 'default-thumbnail',

        ]);
        DB::table('patientregistrations')->insert([
        	'patient_code' => '4',
        	'first_name' => 'suman',
        	'sur_name' => 'mali',
            'DOB' => '2010-08-20',
            'age' => '26',
            'gender' => 'female',
            'join_date' => '2025-08-26',
            'permanent_address' => 'ktm',
            'home_number' => '21',
            'mobile_no' => '9841268525',
            'birthplace' => 'thecho',
            'occupation' => 'BIT',
            'brought_by' => 'nakul',
            'brought_phone_no' => '984311087',
            'patient_email' => 'suman@gmail.com',
            'temp_address' => 'balkot',
            'emergency_no' => '981512212',
            'office_address' => 'lalaitpur',
            'chronic_diseases' => 'migrane',
            'office_number' => '014011101',
            'photo' => 'default-thumbnail',

        ]);
        DB::table('patientregistrations')->insert([
        	'patient_code' => '5',
        	'first_name' => 'utsav',
        	'sur_name' => 'lama',
            'DOB' => '2012-08-20',
            'age' => '25',
            'gender' => 'female',
            'join_date' => '2022-08-26',
            'permanent_address' => 'patan',
            'home_number' => '22',
            'mobile_no' => '9841268532',
            'birthplace' => 'gatthaghar',
            'occupation' => 'MIT',
            'brought_by' => 'ashishnepal',
            'brought_phone_no' => '984311007',
            'patient_email' => 'ashish@gmail.com',
            'temp_address' => 'sukedhara',
            'emergency_no' => '981512112',
            'office_address' => 'kathmandu',
            'chronic_diseases' => 'cancer',
            'office_number' => '014011101',
            'photo' => 'default-thumbnail',

        ]);
    }
}
