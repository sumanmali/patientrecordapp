<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        RolesTableSeeder::class,
        UserTableSeeder::class,
        GenderTableSeeder::class,
        PatientSeeder::class,
        DepartmentTableSeeder::class,
        TestCategorySeeder::class,
        TestSeeder::class,
        doctorTableSeeder::class,
    ]);
    }
}