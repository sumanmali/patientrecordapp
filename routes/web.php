<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/patientregistration','PatientregistrationController@index');
Route::get('/patientregistration/create','PatientregistrationController@create');
Route::post('/patientregistration/create','PatientregistrationController@store');
Route:: get('/patientregistration/edit/{id}','PatientregistrationController@edit');
Route:: post('/patientregistration/edit/{id}','PatientregistrationController@update');
Route:: get('/patientregistration/delete/{id}','PatientregistrationController@destroy');

Route::post('/getPatient', 'PatientregistrationController@getById');
Route::get('/home', 'PatientregistrationController@getPatient');
Route::get('/getPatient', 'PatientregistrationController@getPatient');
Route::post('/newOpdRegistration', 'PatientregistrationController@opdRegistration');

Auth::routes();



Route::post('/rxentry', 'RxController@store');
Route::post('/rxentry/edit/{id}', 'RxController@update');
Route::delete('/rxentry/delete/{id}', 'RxController@destroy');


Route::post('/oesubmit', 'OnExaminationController@store');
Route::post('/oeexamination', 'OnExaminationController@oeexaminationn');
Route::delete('/o_e/delete/{id}', 'OnExaminationController@destroy');
Route::post('/o_e/edit/{id}', 'OnExaminationController@update');


Route::post('/pathology', 'PathologyController@store');

Route::get('/pathology/print/{patient_id}', 'HomeController@printReport');

Route::get('/departments','DepartmentController@index');
Route::get('/department/create', 'DepartmentController@create');
Route::post('/department/create', 'DepartmentController@store');
Route::get('/department/edit/{id}', 'DepartmentController@edit')->name('department.edit');
Route::post('/department/edit/{id}', 'DepartmentController@update')->name('department.update');
Route::delete('/department/destroy/{id}', 'DepartmentController@destroy')->name('department.delete');

Route::get('/test-category','TestCategoryController@index');
Route::get('/test-category/create', 'TestCategoryController@create');
Route::post('/test-category/create', 'TestCategoryController@store');
Route::get('/test-category/edit/{id}', 'TestCategoryController@edit')->name('test-category.edit');
Route::post('/test-category/edit/{id}', 'TestCategoryController@update')->name('test-category.update');
Route::delete('/test-category/destroy/{id}', 'TestCategoryController@destroy')->name('test-category.delete');

Route::get('/tests','TestController@index');
Route::get('/test/create', 'TestController@create');
Route::post('/test/create', 'TestController@store');
Route::get('/test/edit/{id}', 'TestController@edit')->name('test.edit');
Route::post('/test/edit/{id}', 'TestController@update')->name('test.update');
Route::delete('/test/destroy/{id}', 'TestController@destroy')->name('test.delete');


Route::get('/user-management', 'PatientregistrationController@userManagement');

Route::resource('doctors', 'DoctorsController');


Route::post('/cosubmit', 'ChiefComplaintsController@store');
Route::delete('/c_o/delete/{id}', 'ChiefComplaintsController@destroy');
Route::post('/c_o/edit/{id}', 'ChiefComplaintsController@update');

Route::post('/followup', 'FollowUpController@store');
Route::post('/historyIllness', 'HistoryIllnessController@store');
Route::delete('/historyIllness/delete/{id}', 'HistoryIllnessController@destroy');
Route::post('/history/edit/{id}', 'HistoryIllnessController@update');


Route::post('/investigation', 'InvestigationController@store');
Route::delete('/investigation/delete/{id}', 'InvestigationController@destroy');
Route::post('/investigation/edit/{id}', 'InvestigationController@update');

Route::get('/roleChange/{user_id}/{role_id}', 'UserController@assign');
Route::delete('/user/delete/{id}', 'UserController@destroy');

Route::get('/registered', 'HomeController@registered');



Route::get('/prescription/print/{patient_id}', 'HomeController@printPescription');
Route::get('/exportPdf', 'HomeController@exportPdf');
Route::get('/receipt/print/{patient_id}', 'HomeController@printReceipt');


Route::get('/overallReport', 'ReportGenerationController@overallReport');

// Route::get('/print', function(){
// 	return view('template.printTemplate');
// });