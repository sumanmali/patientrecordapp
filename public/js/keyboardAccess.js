function tryfunc(value){
// alert(value);
var tableId = value;
localStorage.setItem('tableId', value);

var grid = document.getElementById(tableId);
SelectRow(grid.rows[1], 1);

}
var SelectedRow = null;
        var SelectedRowIndex = -1;
        var UpperBound = 0;
        var LowerBound = 0;
        var count = 0;

        window.onload = function () {
            
            LowerBound = 1;
            SelectedRowIndex = -1;

            var grid = document.getElementById(localStorage.getItem('tableId'));
            UpperBound = grid.rows.length - 1;
            // alert(grid.rows.length - 1);
            SelectRow(grid.rows[1], 1);

        }
    

        function SelectRow(CurrentRow, RowIndex) {

            if (SelectedRow == CurrentRow || RowIndex > UpperBound || RowIndex < LowerBound)
                return;

            if (SelectedRow != null) {

                SelectedRow.style.backgroundColor = SelectedRow.originalBackgroundColor;
                SelectedRow.style.color = SelectedRow.originalForeColor;
            }

            if (CurrentRow != null) {

                CurrentRow.originalBackgroundColor = CurrentRow.style.backgroundColor;
                CurrentRow.originalForeColor = CurrentRow.style.color;
                CurrentRow.style.backgroundColor = '#C1C1C1';
                CurrentRow.style.color = 'Black';

            }

            SelectedRow = CurrentRow;
            SelectedRowIndex = RowIndex;
            setTimeout("SelectedRow.focus();", 0);
        }

function SelectSibling(e) {
        	// alert("Called");
            var e = e ? e : window.event;
            var KeyCode = e.which ? e.which : e.keyCode;
            var GridView = document.getElementById(localStorage.getItem('tableId'));
            

            var grid = document.getElementById(localStorage.getItem('tableId'));
                Row = grid.rows[SelectedRowIndex];

                code = Row.cells[0].innerHTML;


            if (KeyCode == 40) {
                // alert(SelectedRowIndex);
                SelectRow(SelectedRow.nextSibling, SelectedRowIndex + 1);
            }

            else if (KeyCode == 38) {
                // alert(SelectedRowIndex);
                SelectRow(SelectedRow.previousSibling, SelectedRowIndex - 1);
            }
            else if(KeyCode == 13){
            
                // alert('editForm' + code);
                var modal = document.getElementById('editForm' + code);

                modal.style.display = 'block';
                modal.classList.add('show');
                
                modal.style.backgroundColor = '#000000b8';
                document.body.classList.add('modal-open');
                var closeModal = document.getElementsByClassName('close');
                for(var i = 0; i < closeModal.length; i++){
                    closeModal[i].addEventListener('click', function(){
                        modal.style.display = 'none';
                        modal.classList.remove('show');
                        modal.style.backgroundColor = 'transparent';
                        document.body.classList.remove('modal-open');
                    });
                }
            }
            else if(e.keyCode === 46){
                var winLoc = window.location.href.split("#").pop();
                switch(winLoc){
                    case 'history_illness-tab': $('#historyDltBtn' + code).click();
                    break;
                    case 'c_o-tab': $('#coDltBtn' + code).click();
                    break;
                    case 'rx-tab': $('#deleteDataBtn' + code).click();
                    break;
                    case 'investigation-tab': $('#investDltBtn' + code).click();
                    break;
                    case 'user_management-tab': $('#userDeleteBtn' + code).click();
                    break;
                }    
            }
            return false;
        }
        