var map = { 18: false, 67: false, 72: false, 78: false, 73: false, 82: false, 73: false, 76: false, 79: false, 83: false, 65: false, 84: false };
        $(document).keydown(function (e) {
            var currentUrl = window.location.href;
            if (e.keyCode in map) {
                map[e.keyCode] = true;
                if (map[18] && map[67]) {
                    window.location.href = currentUrl + '#c_o-tab';
                    refresh();
                    tryfunc('coTable');
                }
                else if (map[18] && map[73]) {
                    window.location.href = currentUrl + "#investigation-tab";
                    refresh();
                    tryfunc('investigationTable');
                }
                else if (map[18] && map[72]) {
                    window.location.href = currentUrl + "#history_illness-tab";
                    refresh();
                    tryfunc('historyTable');
                }
                else if (map[18] && map[78]) {
                    window.location.href = currentUrl + "#oe-tab";
                    refresh();
                }
                else if (map[18] && map[65]) {
                    window.location.href = currentUrl + "#pathology-tab";
                    refresh();
                    tryfunc('pathologyTable');
                }
                else if (map[18] && map[82]) {
                    window.location.href = currentUrl + "#rx-tab";
                    refresh();
                    tryfunc('rxTable');
                }
                else if (map[18] && map[76]) {
                    window.location.href = currentUrl + "#follow_up-tab";
                    refresh();
                }
                else if (map[18] && map[83]) {
                    window.location.href = currentUrl + "#selection-tab";
                    refresh();
                    tryfunc('patientList');
                }
                else if (map[18] && map[79]) {
                    window.location.href = currentUrl + "#opd-tab";
                    refresh();
                    tryfunc('opTable');
                }
                else if (map[18] && map[84]) {
                    window.location.href = currentUrl + "#report-tab";
                    refresh();
                    tryfunc('opTable');
                }
            }
        }).keyup(function (e) {
            if (e.keyCode in map) {
                map[e.keyCode] = false;
            }
        });

        function refresh(){
            window.location.reload();
        }