$tabName = window.location.href.split("#").pop();
// console.log($tabName);
if($tabName !== 'http://patient.nepgeeks.com/home' && $tabName !== 'http://patient.nepgeeks.com/getPatient'){
	$tabContent = $tabName.substr(0, $tabName.indexOf('-') );
	links = document.getElementsByClassName('nav-link');
	panel = document.getElementsByClassName('tab-pane');
	for(var i= 0; i < links.length ; i++){
	    links[i].classList.remove("active", "show" );
	}
	for(var i= 0; i < panel.length ; i++){
	    panel[i].classList.remove("active", "show" );
	}
	document.getElementById($tabName).classList.add("active", "show" );
	document.getElementById($tabContent).classList.add("active", "show" );
}