function confirmation(evt){
		result = confirm('Are you sure you want to delete?');
		if(!result){
			evt.preventDefault();
		}
	}

function confirmPrevilege(evt){
		result = confirm('Are you sure you want to give role?');
		if(!result){
			evt.preventDefault();
		}
	}

function readUrl(event){
		var output = document.getElementById('investigationReport');
		var edit = document.getElementById('investigationReportEdit');
   		output.src = URL.createObjectURL(event.target.files[0]);
   		edit.src = URL.createObjectURL(event.target.files[0]);

	}